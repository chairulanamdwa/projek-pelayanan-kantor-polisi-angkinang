<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbSperdamaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_sperdamaian', function (Blueprint $table) {
            $table->bigIncrements('id_sperdamaian');
            $table->integer('id_pelapor');
            $table->integer('id_pelapor2');
            $table->json('data');
            $table->integer('id_user');
            $table->integer('id_petugas')->nullable();
            $table->enum('status', ['proses', 'diselidiki', 'selesai']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb__sperdamaian');
    }
}
