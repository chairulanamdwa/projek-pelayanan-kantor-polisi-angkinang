<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPengaduanMasyarakatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pengaduan_masyarakat', function (Blueprint $table) {
            $table->bigIncrements('id_pengaduan_masyarakat');
            $table->integer('id_pelapor');
            $table->json('data');
            $table->integer('id_user');
            $table->integer('id_petugas')->nullable();
            $table->enum('status', ['proses', 'diselidiki', 'selesai']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pengaduan_masyarakat');
    }
}
