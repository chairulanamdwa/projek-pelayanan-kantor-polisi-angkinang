<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasyarakatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_masyarakat', function (Blueprint $table) {
            $table->bigIncrements('id_masyarakat');
            $table->string('nik', 255);
            $table->string('kk', 255);
            $table->string('nama', 255);
            $table->string('tmpt_lhr', 255);
            $table->date('tgl_lhr');
            $table->enum('jk', ['L', 'P']);
            $table->string('agama', 255);
            $table->string('pekerjaan', 255);
            $table->string('telpon', 255)->nullable();
            $table->string('gol_darah', 2)->nullable();
            $table->string('pddk_terakhir', 255);
            $table->string('stts_kawin', 255);
            $table->text('alamat');
            $table->string('rt', 5);
            $table->string('rw', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_masyarakat');
    }
}
