<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbLpModelBTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_lp_model_b', function (Blueprint $table) {
            $table->bigIncrements('id_lp_model_b');
            $table->integer('id_pelapor');
            $table->string('hari_kejadian', 255);
            $table->date('tgl_kejadian');
            $table->string('pukul_kejadian', 255);
            $table->string('tmpt_kejadian', 255);
            $table->text('apa_yg_terjadi');
            $table->string('hari_lapor', 255);
            $table->date('tgl_lapor');
            $table->string('pukul_lapor', 255);
            $table->text('uraian');
            $table->text('barang_bukti');
            $table->json('data');
            $table->integer('id_user');
            $table->integer('id_petugas')->nullable();
            $table->enum('status', ['proses', 'diselidiki', 'selesai']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_lp_model_b');
    }
}
