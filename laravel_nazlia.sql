-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Bulan Mei 2020 pada 16.42
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_nazlia`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(15, '2020_05_13_065833_add_role_to_users_table', 7),
(31, '2020_05_14_104825_create_tb_masyarakat_table', 9),
(32, '2020_05_15_123814_create_tb_lp_model_a_table', 10),
(33, '2020_05_18_150841_create_tb_lp_model_b_table', 11),
(34, '2020_05_20_050609_create_tb_sktlk_table', 12),
(35, '2020_05_22_084627_create_tb_sik_table', 13),
(40, '2020_05_25_153050_create_tb_sttlp_table', 14),
(41, '2020_05_25_200954_create_tb_skck_table', 14),
(42, '2020_05_26_191929_create_tb__sperdamaian_table', 15),
(44, '2020_05_26_201042_create_tb_pengaduan_masyarakat_table', 16);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_lp_model_a`
--

CREATE TABLE `tb_lp_model_a` (
  `id_lp_model_a` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` int(11) NOT NULL,
  `hari_kejadian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_kejadian` date NOT NULL,
  `pukul_kejadian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmpt_kejadian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apa_yg_terjadi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hari_lapor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lapor` date NOT NULL,
  `pukul_lapor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uraian` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `barang_bukti` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_lp_model_a`
--

INSERT INTO `tb_lp_model_a` (`id_lp_model_a`, `id_pelapor`, `hari_kejadian`, `tgl_kejadian`, `pukul_kejadian`, `tmpt_kejadian`, `apa_yg_terjadi`, `hari_lapor`, `tgl_lapor`, `pukul_lapor`, `uraian`, `barang_bukti`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Senin', '2020-05-18', '12:12', 'Kandangan', 'Pemukulan', 'Senin', '2020-05-18', '11:01', 'Dsadasdasd', 'asdasdasd', '\"{\\\"nama_terlapor\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_terlapor\\\":\\\"Kandangan\\\",\\\"tgl_lhr_terlapor\\\":\\\"1997-06-16\\\",\\\"jk_terlapor\\\":\\\"L\\\",\\\"pekerjaan_terlapor\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_terlapor\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"telpon_terlapor\\\":\\\"082217380171\\\",\\\"nama_1\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_1\\\":\\\"Kandangan\\\",\\\"tgl_lhr_1\\\":\\\"1997-06-16\\\",\\\"jk_1\\\":\\\"L\\\",\\\"pekerjaan_1\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_1\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"agama_1\\\":\\\"Islam\\\",\\\"nama_2\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_2\\\":\\\"Kandangan\\\",\\\"tgl_lhr_2\\\":\\\"1997-06-16\\\",\\\"jk_2\\\":\\\"L\\\",\\\"pekerjaan_2\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_2\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"agama_2\\\":\\\"Islam\\\"}\"', 1, 1, 'selesai', '2020-05-18 21:55:48', '2020-05-19 22:00:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_lp_model_b`
--

CREATE TABLE `tb_lp_model_b` (
  `id_lp_model_b` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` int(11) NOT NULL,
  `hari_kejadian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_kejadian` date NOT NULL,
  `pukul_kejadian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmpt_kejadian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apa_yg_terjadi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hari_lapor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lapor` date NOT NULL,
  `pukul_lapor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uraian` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `barang_bukti` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_lp_model_b`
--

INSERT INTO `tb_lp_model_b` (`id_lp_model_b`, `id_pelapor`, `hari_kejadian`, `tgl_kejadian`, `pukul_kejadian`, `tmpt_kejadian`, `apa_yg_terjadi`, `hari_lapor`, `tgl_lapor`, `pukul_lapor`, `uraian`, `barang_bukti`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Senin', '2020-05-27', '03:03', 'Kandangan', 'wewe', 'Senin', '2020-05-27', '03:04', 'asda', 'sdasdasd', '\"{\\\"nama_terlapor\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_terlapor\\\":\\\"Kandangan\\\",\\\"tgl_lhr_terlapor\\\":\\\"1997-06-16\\\",\\\"jk_terlapor\\\":\\\"L\\\",\\\"pekerjaan_terlapor\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_terlapor\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"telpon_terlapor\\\":\\\"082217380171\\\",\\\"nama_1\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_1\\\":\\\"Kandangan\\\",\\\"tgl_lhr_1\\\":\\\"1997-06-16\\\",\\\"jk_1\\\":\\\"L\\\",\\\"pekerjaan_1\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_1\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"agama_1\\\":\\\"Islam\\\",\\\"nama_2\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_2\\\":\\\"Kandangan\\\",\\\"tgl_lhr_2\\\":\\\"1997-06-16\\\",\\\"jk_2\\\":\\\"L\\\",\\\"pekerjaan_2\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_2\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"agama_2\\\":\\\"Islam\\\"}\"', 1, 1, 'selesai', '2020-05-28 02:59:47', '2020-05-28 03:00:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_masyarakat`
--

CREATE TABLE `tb_masyarakat` (
  `id_masyarakat` bigint(20) UNSIGNED NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmpt_lhr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lhr` date NOT NULL,
  `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL,
  `agama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telpon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gol_darah` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pddk_terakhir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stts_kawin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rt` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rw` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_masyarakat`
--

INSERT INTO `tb_masyarakat` (`id_masyarakat`, `nik`, `kk`, `nama`, `tmpt_lhr`, `tgl_lhr`, `jk`, `agama`, `pekerjaan`, `telpon`, `gol_darah`, `pddk_terakhir`, `stts_kawin`, `alamat`, `rt`, `rw`, `created_at`, `updated_at`) VALUES
(1, '1234', '12341234', 'Khairul Anam', 'Kandangan', '1997-06-16', 'L', 'Islam', 'Developer/Programmer', '082217380171', 'O', 'S1 Teknik Informatika', 'Kawin', 'Desa Tambak Bitin Kec.Daha Utara', '04', '02', '2020-05-18 21:54:23', '2020-05-18 21:54:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pengaduan_masyarakat`
--

CREATE TABLE `tb_pengaduan_masyarakat` (
  `id_pengaduan_masyarakat` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` int(11) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_pengaduan_masyarakat`
--

INSERT INTO `tb_pengaduan_masyarakat` (`id_pengaduan_masyarakat`, `id_pelapor`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, '\"{\\\"hari_kejadian\\\":\\\"Senin\\\",\\\"tgl_kejadian\\\":\\\"2020-05-27\\\",\\\"pukul_kejadian\\\":\\\"03:03\\\",\\\"tmpt_kejadian\\\":\\\"Kandangan\\\",\\\"apa_yg_terjadi\\\":\\\"wewe\\\",\\\"hari_lapor\\\":\\\"Senin\\\",\\\"tgl_lapor\\\":\\\"2020-05-27\\\",\\\"pukul_lapor\\\":\\\"03:03\\\",\\\"uraian\\\":\\\"wer\\\",\\\"nama_terlapor\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_terlapor\\\":\\\"Kandangan\\\",\\\"tgl_lhr_terlapor\\\":\\\"1997-06-16\\\",\\\"jk_terlapor\\\":\\\"L\\\",\\\"pekerjaan_terlapor\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_terlapor\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"telpon_terlapor\\\":\\\"082217380171\\\",\\\"nik_1\\\":\\\"1234\\\",\\\"nama_1\\\":\\\"Khairul Anam\\\",\\\"tmpt_lhr_1\\\":\\\"Kandangan\\\",\\\"tgl_lhr_1\\\":\\\"1997-06-16\\\",\\\"jk_1\\\":\\\"L\\\",\\\"pekerjaan_1\\\":\\\"Developer\\\\\\/Programmer\\\",\\\"alamat_1\\\":\\\"Desa Tambak Bitin Kec.Daha Utara\\\",\\\"agama_1\\\":\\\"Islam\\\"}\"', 1, 1, 'selesai', '2020-05-28 03:22:15', '2020-05-28 03:25:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sik`
--

CREATE TABLE `tb_sik` (
  `id_sik` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` int(11) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_sik`
--

INSERT INTO `tb_sik` (`id_sik`, `id_pelapor`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, '\"{\\\"organisasi\\\":\\\"asd\\\",\\\"penanggung_jawab\\\":\\\"asd\\\",\\\"perihal\\\":\\\"asd\\\",\\\"macam\\\":\\\"asd\\\",\\\"tgl_pelaksanaan\\\":\\\"2020-05-25\\\",\\\"tempat\\\":\\\"asd\\\",\\\"rangka\\\":\\\"asd\\\",\\\"peserta\\\":\\\"asd\\\"}\"', 1, 1, 'selesai', '2020-05-25 22:16:01', '2020-05-25 22:18:59'),
(4, 1, '\"{\\\"organisasi\\\":\\\"dasd\\\",\\\"penanggung_jawab\\\":\\\"asd\\\",\\\"perihal\\\":\\\"asd\\\",\\\"macam\\\":\\\"asd\\\",\\\"tgl_pelaksanaan\\\":\\\"2020-05-27\\\",\\\"tempat\\\":\\\"asd\\\",\\\"rangka\\\":\\\"asd\\\",\\\"peserta\\\":\\\"asd\\\"}\"', 1, 1, 'selesai', '2020-05-27 16:17:24', '2020-05-27 16:27:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_skck`
--

CREATE TABLE `tb_skck` (
  `id_skck` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` int(11) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_skck`
--

INSERT INTO `tb_skck` (`id_skck`, `id_pelapor`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '\"{\\\"data_sidik_jari_1\\\":\\\"asd\\\",\\\"data_sidik_jari_2\\\":\\\"asd\\\",\\\"tanggal_berada_indonesia\\\":\\\"1997-06-16\\\",\\\"sampai\\\":\\\"2020-05-27\\\",\\\"keperluan\\\":\\\"asd\\\",\\\"gambar\\\":\\\"564386348.jpeg\\\"}\"', 1, 1, 'selesai', '2020-05-28 03:18:00', '2020-05-28 03:18:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sktlk`
--

CREATE TABLE `tb_sktlk` (
  `id_sktlk` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_sktlk`
--

INSERT INTO `tb_sktlk` (`id_sktlk`, `id_pelapor`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(2, '1', '\"{\\\"keterangan_barang\\\":\\\"asd\\\",\\\"tempat_tercecer\\\":\\\"asd\\\"}\"', 1, 1, 'selesai', '2020-05-22 02:45:12', '2020-05-22 15:43:12'),
(3, '1', '\"{\\\"keterangan_barang\\\":\\\"awe\\\",\\\"tempat_tercecer\\\":\\\"qwe\\\"}\"', 1, NULL, 'proses', '2020-05-22 02:49:01', '2020-05-22 02:49:01'),
(4, '1', '\"{\\\"keterangan_barang\\\":\\\"adsa\\\",\\\"tempat_tercecer\\\":\\\"dasd\\\"}\"', 1, 1, 'diselidiki', '2020-05-22 02:49:33', '2020-05-22 15:39:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sperdamaian`
--

CREATE TABLE `tb_sperdamaian` (
  `id_sperdamaian` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` int(11) NOT NULL,
  `id_pelapor2` int(11) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_sperdamaian`
--

INSERT INTO `tb_sperdamaian` (`id_sperdamaian`, `id_pelapor`, `id_pelapor2`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '\"{\\\"tgl_kejadian\\\":\\\"2020-05-26\\\",\\\"tentang\\\":\\\"sdas\\\"}\"', 1, 1, 'selesai', '2020-05-27 02:55:09', '2020-05-27 03:05:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sttlp`
--

CREATE TABLE `tb_sttlp` (
  `id_sttlp` bigint(20) UNSIGNED NOT NULL,
  `id_pelapor` int(11) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data untuk tabel `tb_sttlp`
--

INSERT INTO `tb_sttlp` (`id_sttlp`, `id_pelapor`, `data`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '\"{\\\"curat\\\":\\\"asd\\\",\\\"barang_bukti\\\":\\\"asd\\\"}\"', 1, 1, 'selesai', '2020-05-28 03:14:43', '2020-05-28 03:15:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmpt_lhr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lhr` date NOT NULL,
  `pangkat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('admin','petugas','user') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `nik`, `nip`, `jk`, `tmpt_lhr`, `tgl_lhr`, `pangkat`, `alamat`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Admin', '34', '4234', 'L', 'Kandangan', '1997-06-16', NULL, 'Desa Tambak Bitin', 'chairuluniska@gmail.com', NULL, '$2y$10$TG.kUsQzCdk.PGCOV8FN0eBxIIu9Z5IlRAn8yWz13S364tKJhcYrW', 'HQj79yEP4jVHbjbkeLSYUCILsqpEaaBcB3rvad0qydgwZxNDBXSIk5KFZG2f', '2020-02-03 10:39:41', '2020-05-27 16:54:38', 'admin'),
(5, 'Chairul Anam', '-', '-', 'L', '-', '2020-05-14', '-', '-', 'user@gmail.com', NULL, '$2y$10$dKntTXNhsoX9In82JQE6FuFELZa24cex6qRxyNz4bKXHuCp4GE2IS', NULL, '2020-05-14 14:25:34', '2020-05-14 14:25:34', 'user'),
(6, 'Chairul Anam', '1234134', '1234', 'L', '1234', '2020-05-25', '1234', '1234', 'chairuluniwwska@gmail.com', NULL, '$2y$10$hhb8gEc9cAcBQXjhO91LK.6AMUD.YujQ3s.uXyF56zrojXC6d8GPO', NULL, '2020-05-27 16:58:42', '2020-05-28 03:29:22', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `tb_masyarakat`
--
ALTER TABLE `tb_masyarakat`
  ADD PRIMARY KEY (`id_masyarakat`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `tb_lp_model_a`
--
ALTER TABLE `tb_lp_model_a`
  MODIFY `id_lp_model_a` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_lp_model_b`
--
ALTER TABLE `tb_lp_model_b`
  MODIFY `id_lp_model_b` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_masyarakat`
--
ALTER TABLE `tb_masyarakat`
  MODIFY `id_masyarakat` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_pengaduan_masyarakat`
--
ALTER TABLE `tb_pengaduan_masyarakat`
  MODIFY `id_pengaduan_masyarakat` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_sik`
--
ALTER TABLE `tb_sik`
  MODIFY `id_sik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_skck`
--
ALTER TABLE `tb_skck`
  MODIFY `id_skck` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_sktlk`
--
ALTER TABLE `tb_sktlk`
  MODIFY `id_sktlk` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_sperdamaian`
--
ALTER TABLE `tb_sperdamaian`
  MODIFY `id_sperdamaian` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_sttlp`
--
ALTER TABLE `tb_sttlp`
  MODIFY `id_sttlp` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
