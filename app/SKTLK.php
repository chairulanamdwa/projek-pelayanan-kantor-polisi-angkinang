<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SKTLK extends Model
{
    protected $table = 'tb_sktlk';
    protected $fillable = ['id_pelapor', 'data', 'id_user', 'id_petugas', 'status'];
    protected $casts = [
        'data' => 'array'
    ];
}
