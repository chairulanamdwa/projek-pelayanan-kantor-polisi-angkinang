<?php

namespace App\Http\Controllers;

use App\PengaduanMasyarakat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use WordTemplate;


class PengaduanMasyarakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Pengaduan Masyarakat';
        $data['sidebar'] = 'pelayanan';
        return view('pages.pengaduan-masyarakat.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelapor' => 'required',
            'nama_terlapor' => 'required',
            'tmpt_lhr_terlapor' => 'required',
            'tgl_lhr_terlapor' => 'required',
            'jk_terlapor' => 'required',
            'pekerjaan_terlapor' => 'required',
            'alamat_terlapor' => 'required',
            'telpon_terlapor' => 'required',
            'hari_kejadian' => 'required',
            'tgl_kejadian' => 'required',
            'pukul_kejadian' => 'required',
            'tmpt_kejadian' => 'required',
            'apa_yg_terjadi' => 'required',
            'nik_1' => 'required',
            'nama_1' => 'required',
            'tmpt_lhr_1' => 'required',
            'tgl_lhr_1' => 'required',
            'jk_1' => 'required',
            'pekerjaan_1' => 'required',
            'alamat_1' => 'required',
            'agama_1' => 'required',
            'uraian' => 'required',
        ]);

        PengaduanMasyarakat::create([
            'id_pelapor' => $request->id_pelapor,
            'id_user' => Auth::user()->id,
            'data' => json_encode([
                'hari_kejadian' => $request->hari_kejadian,
                'tgl_kejadian' => $request->tgl_kejadian,
                'pukul_kejadian' => $request->pukul_kejadian,
                'tmpt_kejadian' => $request->tmpt_kejadian,
                'apa_yg_terjadi' => $request->apa_yg_terjadi,
                'hari_lapor' => $request->hari_lapor,
                'tgl_lapor' => $request->tgl_lapor,
                'pukul_lapor' => $request->pukul_lapor,
                'uraian' => $request->uraian,
                'nama_terlapor' => $request->nama_terlapor,
                'tmpt_lhr_terlapor' => $request->tmpt_lhr_terlapor,
                'tgl_lhr_terlapor' => $request->tgl_lhr_terlapor,
                'jk_terlapor' => $request->jk_terlapor,
                'pekerjaan_terlapor' => $request->pekerjaan_terlapor,
                'alamat_terlapor' => $request->alamat_terlapor,
                'telpon_terlapor' => $request->telpon_terlapor,
                'nik_1' => $request->nik_1,
                'nama_1' => $request->nama_1,
                'tmpt_lhr_1' => $request->tmpt_lhr_1,
                'tgl_lhr_1' => $request->tgl_lhr_1,
                'jk_1' => $request->jk_1,
                'pekerjaan_1' => $request->pekerjaan_1,
                'alamat_1' => $request->alamat_1,
                'agama_1' => $request->agama_1,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PengaduanMasyarakat  $pengaduanMasyarakat
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data['title'] = 'Pengaduan Masyarakat';
        $data['sidebar'] = 'laporan';
        $data['PMasyarakat_proses'] = PengaduanMasyarakat::where('status', 'proses')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_pengaduan_masyarakat.id_pelapor')
            ->get();
        $data['PMasyarakat_diselidiki'] = PengaduanMasyarakat::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_pengaduan_masyarakat.id_pelapor')
            ->join('users', 'users.id', 'tb_pengaduan_masyarakat.id_petugas')
            ->get();
        $data['PMasyarakat_selesai'] = PengaduanMasyarakat::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_pengaduan_masyarakat.id_pelapor')
            ->join('users', 'users.id', 'tb_pengaduan_masyarakat.id_petugas')
            ->get();

        return view('pages.pengaduan-masyarakat.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PengaduanMasyarakat  $pengaduanMasyarakat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Pengaduan Masyarakat';
        $data['sidebar'] = 'Pengaduan Masyarakat';
        $data['id'] = $id;
        return view('pages.pengaduan-masyarakat.cek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PengaduanMasyarakat  $pengaduanMasyarakat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PengaduanMasyarakat::where('id_pengaduan_masyarakat', $id)->update([
            'status' => 'selesai'
        ]);
        return redirect()->back()->with('message', 'Pengaduan Masyarakat dengan ID ' . $id . ' Telah selesai diselidiki');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PengaduanMasyarakat  $pengaduanMasyarakat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PengaduanMasyarakat::where('id_pengaduan_masyarakat', $id)->delete();
        return redirect()->back()->with('message', 'Pengaduan Masyarakat dengan ID ' . $id . ' Telah dihapus');
    }

    public function cetak(Request $request, $id)
    {
        $request->validate([
            'no_surat' => 'required',
        ]);

        PengaduanMasyarakat::where('id_pengaduan_masyarakat', $id)->update([
            'status' => 'diselidiki',
            'id_petugas' => Auth::user()->id,
        ]);
        $data = PengaduanMasyarakat::where('id_pengaduan_masyarakat', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_pengaduan_masyarakat.id_pelapor')
            ->first();

        $file = public_path('assets/format-surat/pengaduan-masyarakat.rtf');

        $namaBulan = ["Januari", "Februaru", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        $petugas = Auth::user();

        $array = array(
            '[tahun]' => date('Y'),
            '[no_surat]' => $request->no_surat,
            '[nama_pelapor]' => $data->nama,
            '[ttl_pelapor]' => $data->tmpt_lhr . ',' . date('d-m-Y', strtotime($data->tgl_lhr)),
            '[jk_pelapor]' => $data->jk,
            '[pekerjaan_pelapor]' => $data->pekerjaan,
            '[alamat_pelapor]' => $data->alamat,
            '[telpon_pelapor]' => $data->telpon,

            '[data_hari_kejadian]' => $data->hari_kejadian,

            '[data_tgl_kejadian]' => date('d-m-Y', strtotime($data->tgl_kejadian)),
            '[data_pukul_kejadian]' => $data->pukul_kejadian,
            '[data_tempat_kejadian]' => json_decode($data->data, true)['tmpt_kejadian'],
            '[data_apa_yg_terjadi]' => json_decode($data->data, true)['apa_yg_terjadi'],

            '[nama_terlapor]' => json_decode($data->data, true)['nama_terlapor'],
            '[ttl_terlapor]' => json_decode($data->data, true)['tmpt_lhr_terlapor'] . ',' . date('d-m-Y', strtotime(json_decode($data->data, true)['tgl_lhr_terlapor'])),
            '[jk_terlapor]' => json_decode($data->data, true)['jk_terlapor'],
            '[pekerjaan_terlapor]' => json_decode($data->data, true)['pekerjaan_terlapor'],
            '[alamat_terlapor]' => json_decode($data->data, true)['alamat_terlapor'],
            '[telpon_terlapor]' => json_decode($data->data, true)['telpon_terlapor'],

            '[data_hari]' => json_decode($data->data, true)['hari_kejadian'],
            '[data_tanggal]' => date('d-m-Y', strtotime(json_decode($data->data, true)['tgl_kejadian'])),
            '[data_pukul]' => json_decode($data->data, true)['pukul_kejadian'],

            '[nama_saksi]' =>  json_decode($data->data, true)['nama_1'],
            '[ttl_saksi]' => json_decode($data->data, true)['tmpt_lhr_1'] . ',' . date('d-m-Y', strtotime(json_decode($data->data, true)['tgl_lhr_1'])),
            '[jk_saksi]' => json_decode($data->data, true)['jk_1'],
            '[pekerjaan_saksi]' => json_decode($data->data, true)['pekerjaan_1'],
            '[alamat_saksi]' => json_decode($data->data, true)['alamat_1'],
            '[agama_saksi]' => json_decode($data->data, true)['agama_1'],

            '[data_uraian_kejadian]' => json_decode($data->data, true)['uraian'],

            '[tgl_surat]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'Pengaduan Masyarakat-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);
    }

    public function printSelidiki()
    {
        $data['title'] = 'Pengaduan Masyarakat yang di Selidiki';
        $data['PMasyarakat_diselidiki'] = PengaduanMasyarakat::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_pengaduan_masyarakat.id_pelapor')
            ->join('users', 'users.id', 'tb_pengaduan_masyarakat.id_petugas')
            ->get();
        return view('pages.pengaduan-masyarakat.print_selidiki', $data);
    }

    public function printSelesai()
    {
        $data['title'] = 'Pengaduan Masyarakat yang sudah Selesai';
        $data['PMasyarakat_selesai'] = PengaduanMasyarakat::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_pengaduan_masyarakat.id_pelapor')
            ->join('users', 'users.id', 'tb_pengaduan_masyarakat.id_petugas')
            ->get();
        return view('pages.pengaduan-masyarakat.print_selesai', $data);
    }
}
