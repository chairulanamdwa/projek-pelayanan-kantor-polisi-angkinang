<?php

namespace App\Http\Controllers;

use WordTemplate;
use App\SKTLK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SktlkController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'SKTLK';
        $data['sidebar'] = 'pelayanan';
        return view('pages.sktlk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelapor' => 'required',
            'keterangan_barang' => 'required',
            'tempat_tercecer' => 'required',
        ]);

        SKTLK::create([
            'id_pelapor' => $request->id_pelapor,
            'id_user' => Auth::user()->id,
            'data' => json_encode([
                'keterangan_barang' => $request->keterangan_barang,
                'tempat_tercecer' => $request->tempat_tercecer,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SKTLK  $SKTLK
     * @return \Illuminate\Http\Response
     */
    public function show(SKTLK $SKTLK)
    {
        $data['title'] = 'SKTLK';
        $data['sidebar'] = 'laporan';
        $data['SKTLK_proses'] = SKTLK::where('status', 'proses')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sktlk.id_pelapor')
            ->get();
        $data['SKTLK_diselidiki'] = SKTLK::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sktlk.id_pelapor')
            ->join('users', 'users.id', 'tb_sktlk.id_petugas')
            ->get();
        $data['SKTLK_selesai'] = SKTLK::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sktlk.id_pelapor')
            ->join('users', 'users.id', 'tb_sktlk.id_petugas')
            ->get();
        return view('pages.sktlk.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SKTLK  $SKTLK
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'SKTLK';
        $data['sidebar'] = 'SKTLK';
        $data['id'] = $id;
        return view('pages.sktlk.cek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SKTLK  $SKTLK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        SKTLK::where('id_sktlk', $id)->update([
            'status' => 'selesai'
        ]);
        return redirect()->back()->with('message', 'SKTLK dengan ID ' . $id . ' Telah selesai diselidiki');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SKTLK  $SKTLK
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SKTLK::where('id_sktlk', $id)->delete();
        return redirect()->back()->with('message', 'SKTLK dengan ID ' . $id . ' Telah dihapus');
    }

    public function search(Request $request)
    {
        $from = $request->date1;
        $to = $request->date2;
        $data['laporan'] = SKTLK::whereBetween('created_at', [$from, $to])->get();
        return view('pages.sktlk.search', $data);
    }

    public function cetak(Request $request, $id)
    {

        $request->validate([
            'no_surat' => 'required',
        ]);

        SKTLK::where('id_sktlk', $id)->update([
            'status' => 'diselidiki',
            'id_petugas' => Auth::user()->id,
        ]);

        $data = SKTLK::where('id_sktlk', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sktlk.id_pelapor')
            ->first();


        $namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $hari = date("D");

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        $file = public_path('assets/format-surat/sktlk.rtf');
        $array = array(
            '[tahun]' => date('Y'),
            '[no_surat]' => $request->no_surat,
            '[data_nama]' => $data->nama,
            '[data_ttl]' => $data->tmpt_lhr . ',' . date('d-m-Y', strtotime($data->tgl_lhr)),
            '[data_agama]' => $data->agama,
            '[data_pekerjaan]' => $data->pekerjaan,
            '[data_jk]' => $data->jk,
            '[data_alamat]' => $data->alamat,
            '[data_telp]' => $data->telpon,
            '[data_yang_hilang]' => json_decode($data->data, true)['keterangan_barang'],
            '[data_daerah_hilang]' => json_decode($data->data, true)['tempat_tercecer'],

            '[data_waktu]' => date('H.i'),
            '[data_hari]' => $hari_ini,
            '[data_tanggal]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'sktlk-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);
    }

    public function printSelidiki()
    {
        $data['title'] = 'SKTLK yang di Selidiki';
        $data['SKTLK_diselidiki'] = SKTLK::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sktlk.id_pelapor')
            ->join('users', 'users.id', 'tb_sktlk.id_petugas')
            ->get();
        return view('pages.sktlk.print_selidiki', $data);
    }

    public function printSelesai()
    {
        $data['title'] = 'SKTLK yang sudah Selesai';
        $data['SKTLK_selesai'] = SKTLK::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sktlk.id_pelapor')
            ->join('users', 'users.id', 'tb_sktlk.id_petugas')
            ->get();
        return view('pages.sktlk.print_selesai', $data);
    }
}
