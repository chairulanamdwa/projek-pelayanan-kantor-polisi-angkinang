<?php

namespace App\Http\Controllers;

use App\SIK;
use WordTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SIKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'SIK';
        $data['sidebar'] = 'pelayanan';
        return view('pages.sik.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelapor' => 'required',
            'organisasi' => 'required',
            'penanggung_jawab' => 'required',
            'perihal' => 'required',
            'macam' => 'required',
            'tgl_pelaksanaan' => 'required',
            'tempat' => 'required',
            'rangka' => 'required',
            'peserta' => 'required',
        ]);

        SIK::create([
            'id_pelapor' => $request->id_pelapor,
            'id_user' => Auth::user()->id,
            'data' => json_encode([
                'organisasi' => $request->organisasi,
                'penanggung_jawab' => $request->penanggung_jawab,
                'perihal' => $request->perihal,
                'macam' => $request->macam,
                'tgl_pelaksanaan' => $request->tgl_pelaksanaan,
                'tempat' => $request->tempat,
                'rangka' => $request->rangka,
                'peserta' => $request->peserta,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SIK  $sIK
     * @return \Illuminate\Http\Response
     */
    public function show(SIK $sIK)
    {
        $data['title'] = 'SIK';
        $data['sidebar'] = 'laporan';
        $data['SIK_proses'] = SIK::where('status', 'proses')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sik.id_pelapor')
            ->get();
        $data['SIK_diselidiki'] = SIK::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sik.id_pelapor')
            ->join('users', 'users.id', 'tb_sik.id_petugas')
            ->get();
        $data['SIK_selesai'] = SIK::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sik.id_pelapor')
            ->join('users', 'users.id', 'tb_sik.id_petugas')
            ->get();

        return view('pages.sik.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SIK  $sIK
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'SIK';
        $data['sidebar'] = 'SIK';
        $data['id'] = $id;
        return view('pages.sik.cek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SIK  $sIK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        SIK::where('id_sik', $id)->update([
            'status' => 'selesai'
        ]);
        return redirect()->back()->with('message', 'SIK dengan ID ' . $id . ' Telah selesai diselidiki');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SIK  $sIK
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SIK::where('id_sik', $id)->delete();
        return redirect()->back()->with('message', 'SIK ID ' . $id . ' Telah dihapus');
    }

    public function search(Request $request)
    {
        $from = $request->date1;
        $to = $request->date2;
        $data['laporan'] = SIK::whereBetween('created_at', [$from, $to])->get();
        return view('pages.sik.search', $data);
    }

    public function cetak(Request $request, $id)
    {

        $request->validate([
            'no_surat' => 'required',
        ]);

        SIK::where('id_sik', $id)->update([
            'status' => 'diselidiki',
            'id_petugas' => Auth::user()->id,
        ]);

        $data = SIK::where('id_sik', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sik.id_pelapor')
            ->first();


        $namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $hari = date("D");

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        $file = public_path('assets/format-surat/sik-1.rtf');
        $array = array(
            '[data_tahun]' => date('Y'),
            '[no_surat]' => $request->no_surat,
            '[data_nama]' => $data->nama,
            '[data_ttl]' => $data->tmpt_lhr . ',' . date('d-m-Y', strtotime($data->tgl_lhr)),
            '[data_agama]' => $data->agama,
            '[data_pekerjaan]' => $data->pekerjaan,
            '[data_jk]' => $data->jk,
            '[data_alamat]' => $data->alamat,
            '[data_telp]' => $data->telpon,
            '[data_perihal]' => json_decode($data->data, true)['perihal'],
            '[data_organisasi]' => json_decode($data->data, true)['organisasi'],
            '[data_penanggung_jawab]' => json_decode($data->data, true)['penanggung_jawab'],
            '[data_macam]' => json_decode($data->data, true)['macam'],
            '[data_hari_dan_tanggal]' => json_decode($data->data, true)['tgl_pelaksanaan'],
            '[data_tempat]' => json_decode($data->data, true)['tempat'],
            '[data_rangka]' => json_decode($data->data, true)['rangka'],
            '[data_peserta]' => json_decode($data->data, true)['peserta'],

            '[data_waktu]' => date('H.i'),
            '[data_hari]' => $hari_ini,
            '[data_tanggal]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'SIK-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);
    }

    public function printSelidiki()
    {
        $data['title'] = 'SIK yang di Selidiki';
        $data['SIK_diselidiki'] = SIK::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sik.id_pelapor')
            ->join('users', 'users.id', 'tb_sik.id_petugas')
            ->get();
        return view('pages.sik.print_selidiki', $data);
    }

    public function printSelesai()
    {
        $data['title'] = 'SIK yang sudah Selesai';
        $data['SIK_selesai'] = SIK::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sik.id_pelapor')
            ->join('users', 'users.id', 'tb_sik.id_petugas')
            ->get();
        return view('pages.sik.print_selesai', $data);
    }
}
