<?php

namespace App\Http\Controllers;

use App\Masyarakat;
use Illuminate\Http\Request;

class MasyarakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Masyarakat';
        $data['sidebar'] = 'Masyarakat';
        $data['masyarakat'] = Masyarakat::all();
        return view('pages.masyarakat.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Masyarakat';
        $data['sidebar'] = 'Masyarakat';
        $data['masyarakat'] = Masyarakat::all();
        return view('pages.masyarakat.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string',
            'nik' => 'required|unique:tb_masyarakat',
            'kk' => 'required',
            'tmpt_lhr' => 'required|string',
            'tgl_lhr' => 'required',
            'jk' => 'required|string',
            'agama' => 'required|string',
            'pekerjaan' => 'required|string',
            'pddk_terakhir' => 'required|string',
            'stts_kawin' => 'required|string',
            'alamat' => 'required|string',
            'rt' => 'required',
            'rw' => 'required',
        ]);

        Masyarakat::create($request->all());

        return redirect()->route('masyarakat')->with('message', 'Data masyarakat berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function show(Masyarakat $masyarakat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Masyarakat';
        $data['sidebar'] = 'Masyarakat';
        $data['masyarakat'] = Masyarakat::where('id_masyarakat', $id)->first();
        return view('pages.masyarakat.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|string',
            'kk' => 'required',
            'tmpt_lhr' => 'required|string',
            'tgl_lhr' => 'required',
            'jk' => 'required|string',
            'agama' => 'required|string',
            'pekerjaan' => 'required|string',
            'pddk_terakhir' => 'required|string',
            'stts_kawin' => 'required|string',
            'alamat' => 'required|string',
            'rt' => 'required',
            'rw' => 'required',
        ]);

        Masyarakat::where('id_masyarakat', $id)->update([
            'nama' => $request->nama,
            'kk' => $request->kk,
            'tmpt_lhr' => $request->tmpt_lhr,
            'tgl_lhr' => $request->tgl_lhr,
            'jk' => $request->jk,
            'agama' => $request->agama,
            'pekerjaan' => $request->pekerjaan,
            'telpon' => $request->telpon,
            'gol_darah' => $request->gol_darah,
            'pddk_terakhir' => $request->pddk_terakhir,
            'stts_kawin' => $request->stts_kawin,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
        ]);

        return redirect()->route('masyarakat')->with('message', 'Data masyarakat berhasil diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Masyarakat::where('id_masyarakat', $id)->delete();
        return redirect()->route('masyarakat')->with('message', 'Data masyarakat berhasil dihapus');
    }

    public function keluarga($kk)
    {
        $data['title'] = 'Masyarakat';
        $data['sidebar'] = 'Masyarakat';
        $data['kk'] = $kk;
        $data['masyarakat'] = Masyarakat::where('kk', $kk)->get();
        return view('pages.masyarakat.keluarga', $data);
    }
}
