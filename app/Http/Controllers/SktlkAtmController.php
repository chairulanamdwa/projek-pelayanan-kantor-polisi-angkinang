<?php

namespace App\Http\Controllers;

use WordTemplate;
use App\SKTLKATM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SktlkAtmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'SKTLK';
        $data['sidebar'] = 'pelayanan';
        return view('pages.sktlk-atm.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tmpt_lhr' => 'required',
            'tgl_lhr' => 'required',
            'agama' => 'required',
            'pekerjaan' => 'required',
            'alamat' => 'required',
            'telpon' => 'required',
            'keterangan_barang' => 'required',
            'tempat_tercecer' => 'required',
        ]);

        SKTLKATM::create([
            'nama' => $request->nama,
            'tmpt_lhr' => $request->tmpt_lhr,
            'tgl_lhr' => $request->tgl_lhr,
            'agama' => $request->agama,
            'pekerjaan' => $request->pekerjaan,
            'alamat' => $request->alamat,
            'keterangan_barang' => $request->keterangan_barang,
            'tempat_tercecer' => $request->tempat_tercecer,
            'id_petugas' => Auth::user()->id,
        ]);

        $namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $hari = date("D");

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        $file = public_path('assets/format-surat/sktlk-atm.rtf');
        $array = array(
            '[tahun]' => date('Y'),
            '[data_nama]' => $request->nama,
            '[data_ttl]' => $request->tmpt_lhr . ',' . date('d-m-Y', strtotime($request->tgl_lhr)),
            '[data_agama]' => $request->agama,
            '[data_pekerjaan]' => $request->pekerjaan,
            '[data_jk]' => $request->jk,
            '[data_alamat]' => $request->alamat,
            '[data_telp]' => $request->telpon,
            '[data_yang_hilang]' => $request->keterangan_barang,
            '[data_daerah_hilang]' => $request->tempat_tercecer,

            '[data_waktu]' => date('H.i'),
            '[data_hari]' => $hari_ini,
            '[data_tanggal]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'sktlk-atm-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);



        return redirect(route('menu-layanan'))->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SKTLKATM  $sKTLKATM
     * @return \Illuminate\Http\Response
     */
    public function show(SKTLKATM $sKTLKATM)
    {
        $data['title'] = 'SKTLK ATM';
        $data['sidebar'] = 'laporan';
        $data['showData'] = SKTLKATM::orderBy('nama', 'ASC')
            ->paginate(20);
        return view('pages.sktlk-atm.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SKTLKATM  $sKTLKATM
     * @return \Illuminate\Http\Response
     */
    public function edit(SKTLKATM $sKTLKATM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SKTLKATM  $sKTLKATM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SKTLKATM $sKTLKATM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SKTLKATM  $sKTLKATM
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        SKTLKATM::where('id_sktlk_atm', $request->data)->delete();
        return "Data Berhasil di Hapus";
    }

    public function search(Request $request)
    {
        $from = $request->date1;
        $to = $request->date2;
        $data['laporan'] = SKTLKATM::whereBetween('created_at', [$from, $to])->get();
        return view('pages.sktlk-atm.search', $data);
    }
}
