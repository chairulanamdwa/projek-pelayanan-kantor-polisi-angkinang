<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserProfilController extends Controller
{

    public function index()
    {
        $data['title'] = 'profil';
        $data['sidebar'] = 'profil';
        return view('pages.user.index', $data);
    }

    public function update(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'jk' => 'required',
            'tmpt_lhr' => 'required',
            'tgl_lhr' =>  'required',
            'alamat' => 'required',
        ]);

        User::where('id', Auth::user()->id)->update([
            'name' => $request->name,
            'nip' => $request->nip,
            'jk' => $request->jk,
            'tmpt_lhr' => $request->tmpt_lhr,
            'tgl_lhr' => $request->tgl_lhr,
            'pangkat' => $request->pangkat,
            'alamat' => $request->alamat,
        ]);

        return redirect()->back()->with('message', 'Profil berhasil di perbaharui');
    }
}
