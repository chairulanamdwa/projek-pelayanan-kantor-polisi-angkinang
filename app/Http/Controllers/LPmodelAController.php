<?php

namespace App\Http\Controllers;

use WordTemplate;
use App\LPModelA;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LPmodelAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'LP Model A';
        $data['sidebar'] = 'LP Model A';
        return view('pages.lp-model-a.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'id_pelapor' => 'required',
            'nama_terlapor' => 'required',
            'tmpt_lhr_terlapor' => 'required',
            'tgl_lhr_terlapor' => 'required',
            'jk_terlapor' => 'required',
            'pekerjaan_terlapor' => 'required',
            'alamat_terlapor' => 'required',
            'telpon_terlapor' => 'required',
            'hari_kejadian' => 'required',
            'tgl_kejadian' => 'required',
            'pukul_kejadian' => 'required',
            'tmpt_kejadian' => 'required',
            'apa_yg_terjadi' => 'required',
            'hari_lapor' => 'required',
            'tgl_lapor' => 'required',
            'pukul_lapor' => 'required',
            'nama_1' => 'required',
            'tmpt_lhr_1' => 'required',
            'tgl_lhr_1' => 'required',
            'jk_1' => 'required',
            'pekerjaan_1' => 'required',
            'alamat_1' => 'required',
            'agama_1' => 'required',
            'nama_2' => 'required',
            'tmpt_lhr_2' => 'required',
            'tgl_lhr_2' => 'required',
            'jk_2' => 'required',
            'pekerjaan_2' => 'required',
            'alamat_2' => 'required',
            'agama_2' => 'required',
            'uraian' => 'required',
            'barang_bukti' => 'required',
        ]);

        LPModelA::create([
            'id_pelapor' => $request->id_pelapor,
            'id_user' => Auth::user()->id,
            'hari_kejadian' => $request->hari_kejadian,
            'tgl_kejadian' => $request->tgl_kejadian,
            'pukul_kejadian' => $request->pukul_kejadian,
            'tmpt_kejadian' => $request->tmpt_kejadian,
            'apa_yg_terjadi' => $request->apa_yg_terjadi,
            'hari_lapor' => $request->hari_lapor,
            'tgl_lapor' => $request->tgl_lapor,
            'pukul_lapor' => $request->pukul_lapor,
            'uraian' => $request->uraian,
            'barang_bukti' => $request->barang_bukti,
            'data' => json_encode([
                'nama_terlapor' => $request->nama_terlapor,
                'tmpt_lhr_terlapor' => $request->tmpt_lhr_terlapor,
                'tgl_lhr_terlapor' => $request->tgl_lhr_terlapor,
                'jk_terlapor' => $request->jk_terlapor,
                'pekerjaan_terlapor' => $request->pekerjaan_terlapor,
                'alamat_terlapor' => $request->alamat_terlapor,
                'telpon_terlapor' => $request->telpon_terlapor,
                'nama_1' => $request->nama_1,
                'tmpt_lhr_1' => $request->tmpt_lhr_1,
                'tgl_lhr_1' => $request->tgl_lhr_1,
                'jk_1' => $request->jk_1,
                'pekerjaan_1' => $request->pekerjaan_1,
                'alamat_1' => $request->alamat_1,
                'agama_1' => $request->agama_1,
                'nama_2' => $request->nama_2,
                'tmpt_lhr_2' => $request->tmpt_lhr_2,
                'tgl_lhr_2' => $request->tgl_lhr_2,
                'jk_2' => $request->jk_2,
                'pekerjaan_2' => $request->pekerjaan_2,
                'alamat_2' => $request->alamat_2,
                'agama_2' => $request->agama_2,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LPModelA  $lPModelA
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data['title'] = 'LP Model A';
        $data['sidebar'] = 'laporan';
        $data['LPMA_proses'] = LPModelA::where('status', 'proses')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_lp_model_a.id_pelapor')
            ->get();
        $data['LPMA_diselidiki'] = LPModelA::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_lp_model_a.id_pelapor')
            ->join('users', 'users.id', 'tb_lp_model_a.id_petugas')
            ->get();
        $data['LPMA_selesai'] = LPModelA::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_lp_model_a.id_pelapor')
            ->join('users', 'users.id', 'tb_lp_model_a.id_petugas')
            ->get();

        return view('pages.lp-model-a.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LPModelA  $lPModelA
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'LP Model A';
        $data['sidebar'] = 'LP Model A';
        $data['id'] = $id;
        return view('pages.lp-model-a.cek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LPModelA  $lPModelA
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        LPModelA::where('id_lp_model_a', $id)->update([
            'status' => 'selesai'
        ]);
        return redirect()->back()->with('message', 'LP Model A dengan ID ' . $id . ' Telah selesai diselidiki');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LPModelA  $lPModelA
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LPModelA::where('id_lp_model_a', $id)->delete();
        return redirect()->back()->with('message', 'Data berhasil dihapus');
    }

    public function cetak(Request $request, $id)
    {
        $request->validate([
            'no_surat' => 'required',
        ]);

        LPModelA::where('id_lp_model_a', $id)->update([
            'status' => 'diselidiki',
            'id_petugas' => Auth::user()->id,
        ]);
        $data = LPModelA::where('id_lp_model_a', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_lp_model_a.id_pelapor')
            ->first();

        $file = public_path('assets/format-surat/lp-model-a.rtf');

        $namaBulan = ["Januari", "Februaru", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        $array = array(
            '[data_tahun]' => date('Y'),
            '[no_surat]' => $request->no_surat,
            '[data_nama_pelapor]' => $data->nama,
            '[data_ttl_pelapor]' => $data->tmpt_lhr . ',' . date('d-m-Y', strtotime($data->tgl_lhr)),
            '[data_jk_pelapor]' => $data->jk,
            '[data_pekerjaan_pelapor]' => $data->pekerjaan,
            '[data_alamat_pelapor]' => $data->alamat,
            '[data_telp_pelapor]' => $data->telpon,
            '[data_nik_pelapor]' => $data->nik,

            '[data_hari_kejadian]' => $data->hari_kejadian,
            '[data_tgl_kejadian]' => date('d-m-Y', strtotime($data->tgl_kejadian)),
            '[data_pukul_kejadian]' => $data->pukul_kejadian,
            '[data_tmpt_kejadian]' => $data->tmpt_kejadian,
            '[data_apa_yg_terjadi]' => $data->apa_yg_terjadi,
            '[data_nama_terlapor]' => json_decode($data->data, true)['nama_terlapor'],
            '[data_ttl_terlapor]' => json_decode($data->data, true)['tmpt_lhr_terlapor'] . ',' . date('d-m-Y', strtotime(json_decode($data->data, true)['tgl_lhr_terlapor'])),
            '[data_jk_terlapor]' => json_decode($data->data, true)['jk_terlapor'],
            '[data_pekerjaan_terlapor]' => json_decode($data->data, true)['pekerjaan_terlapor'],
            '[data_alamat_terlapor]' => json_decode($data->data, true)['alamat_terlapor'],

            '[data_hari_lapor]' => $data->hari_lapor,
            '[data_tgl_lapor]' => date('d-m-Y', strtotime($data->tgl_lapor)),
            '[data_pukul_lapor]' => $data->pukul_lapor,

            '[data_nama_1]' =>  json_decode($data->data, true)['nama_1'],
            '[data_ttl_1]' => json_decode($data->data, true)['tmpt_lhr_1'] . ',' . date('d-m-Y', strtotime(json_decode($data->data, true)['tgl_lhr_1'])),
            '[data_jk_1]' => json_decode($data->data, true)['jk_1'],
            '[data_pekerjaan_1]' => json_decode($data->data, true)['pekerjaan_1'],
            '[data_alamat_1]' => json_decode($data->data, true)['alamat_1'],
            '[data_agama_1]' => json_decode($data->data, true)['agama_1'],

            '[data_nama_2]' =>  json_decode($data->data, true)['nama_2'],
            '[data_ttl_2]' => json_decode($data->data, true)['tmpt_lhr_2'] . ',' . date('d-m-Y', strtotime(json_decode($data->data, true)['tgl_lhr_2'])),
            '[data_jk_2]' => json_decode($data->data, true)['jk_2'],
            '[data_pekerjaan_2]' => json_decode($data->data, true)['pekerjaan_2'],
            '[data_alamat_2]' => json_decode($data->data, true)['alamat_2'],
            '[data_agama_2]' => json_decode($data->data, true)['agama_2'],


            '[data_uraian]' => $data->uraian,
            '[data_barang_bukti]' => $data->barang_bukti,

            '[tanggal_surat]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'lp-model-a-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);
    }

    public function printSelidiki()
    {
        $data['title'] = 'LP Model A yang di Selidiki';
        $data['LPMA_diselidiki'] = LPModelA::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_lp_model_a.id_pelapor')
            ->join('users', 'users.id', 'tb_lp_model_a.id_petugas')
            ->get();
        return view('pages.lp-model-a.print_selidiki', $data);
    }

    public function printSelesai()
    {
        $data['title'] = 'LP Model A yang sudah Selesai';
        $data['LPMA_selesai'] = LPModelA::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_lp_model_a.id_pelapor')
            ->join('users', 'users.id', 'tb_lp_model_a.id_petugas')
            ->get();

        return view('pages.lp-model-a.print_selesai', $data);
    }
}
