<?php

namespace App\Http\Controllers;

use App\SKCK;
use WordTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;


class SKCKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'SKCK';
        $data['sidebar'] = 'SKCK';
        return view('pages.skck.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'id_pelapor' => 'required',
            'data_sidik_jari_1' => 'required',
            'data_sidik_jari_2' => 'required',
            'tanggal_berada_indonesia' => 'required',
            'sampai' => 'required',
            'keperluan' => 'required',
            'gambar' => 'required',
        ]);

        if ($request->file('gambar')) {
            $file = $request->file('gambar');
            $ekstensi_file1 = $file->getClientOriginalExtension();
            $nama_foto = rand() . '.' . $ekstensi_file1;
            $file->move(public_path('assets/img/upload/'), $nama_foto);
        } else {
            $nama_foto = 'default.png';
        }


        SKCK::create([
            'id_pelapor' => $request->id_pelapor,
            'id_user' => Auth::user()->id,
            'data' => json_encode([
                'data_sidik_jari_1' => $request->data_sidik_jari_1,
                'data_sidik_jari_2' => $request->data_sidik_jari_2,
                'tanggal_berada_indonesia' => $request->tanggal_berada_indonesia,
                'sampai' => $request->sampai,
                'keperluan' => $request->keperluan,
                'gambar' => $nama_foto,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SKCK  $sKCK
     * @return \Illuminate\Http\Response
     */
    public function show(SKCK $sKCK)
    {
        $data['title'] = 'SKCK';
        $data['sidebar'] = 'laporan';
        $data['SKCK_proses'] = SKCK::where('status', 'proses')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_skck.id_pelapor')
            ->get();
        $data['SKCK_diselidiki'] = SKCK::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_skck.id_pelapor')
            ->join('users', 'users.id', 'tb_skck.id_petugas')
            ->get();
        $data['SKCK_selesai'] = SKCK::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_skck.id_pelapor')
            ->join('users', 'users.id', 'tb_skck.id_petugas')
            ->get();


        return view('pages.skck.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SKCK  $sKCK
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'SKCK';
        $data['sidebar'] = 'SKCK';
        $data['id'] = $id;
        return view('pages.skck.cek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SKCK  $sKCK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        SKCK::where('id_skck', $id)->update([
            'status' => 'selesai'
        ]);
        return redirect()->back()->with('message', 'SKCK dengan ID ' . $id . ' Telah selesai diselidiki');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SKCK  $sKCK
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = SKCK::where('id_skck', $id)->first();

        if ($data->gambar != 'default.png') {
            File::delete("public/assets/img/upload/" . json_decode($data->data, true)['gambar']);
        }

        SKCK::where('id_skck', $id)->delete();
        return redirect()->back()->with('message', 'SKCK ID ' . $id . ' Telah dihapus');
    }

    public function search(Request $request)
    {
        $from = $request->date1;
        $to = $request->date2;
        $data['laporan'] = SKCK::whereBetween('created_at', [$from, $to])->get();
        return view('pages.skck.search', $data);
    }

    public function cetak(Request $request, $id)
    {

        $request->validate([
            'no_surat' => 'required',
        ]);

        $data = SKCK::where('id_skck', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_skck.id_pelapor')
            ->first();

        SKCK::where('id_skck', $id)->update([
            'status' => 'selesai',
            'id_petugas' => Auth::user()->id,
        ]);

        $namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $hari = date("D");

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }

        $file = public_path('assets/format-surat/skck.rtf');
        $array = array(
            '[data_tahun]' => date('Y'),
            '[no_surat]' => $request->no_surat,
            '[data_nama]' => $data->nama,
            '[data_jk]' => $data->jk,
            '[data_ttl]' => $data->tmpt_lhr . ',' . date('d-m-Y', strtotime($data->tgl_lhr)),
            '[data_alamat]' => $data->alamat,
            '[data_pekerjaan]' => $data->pekerjaan,
            '[data_nik]' => $data->nik,
            '[data_telp]' => $data->telpon,
            '[data_sidik_jari]' => json_decode($data->data, true)['data_sidik_jari_1'],
            '[data_sidik_jari2]' => json_decode($data->data, true)['data_sidik_jari_2'],
            '[data_tanggal_1]' =>  json_decode($data->data, true)['tanggal_berada_indonesia'],
            '[data_tanggal_2]' => json_decode($data->data, true)['sampai'],
            '[data_keperluan]' => json_decode($data->data, true)['keperluan'],
            '[data_exp]' => date('d-m-Y'),
            '[data_sampai_dengan]' => date('d-m-Y', strtotime('+2 year')),

            '[data_waktu]' => date('H.i'),
            '[data_hari]' => $hari_ini,
            '[data_tanggal]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'SKCK-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);
    }

    public function printSelesai()
    {
        $data['title'] = 'SKCK yang sudah Selesai';
        $data['SKCK_selesai'] = SKCK::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_skck.id_pelapor')
            ->join('users', 'users.id', 'tb_skck.id_petugas')
            ->get();
        return view('pages.skck.print_selesai', $data);
    }
}
