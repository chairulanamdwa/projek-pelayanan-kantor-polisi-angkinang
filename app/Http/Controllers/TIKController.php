<?php

namespace App\Http\Controllers;

use App\TIK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TIKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'TIK';
        $data['sidebar'] = 'pelayanan';
        return view('pages.tik.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelapor' => 'required',
            'curat' => 'required',
            'barang_bukti' => 'required',
        ]);

        TIK::create([
            'id_pelapor' => $request->id_pelapor,
            'id_user' => Auth::user()->id,
            'data' => json_encode([
                'curat' => $request->curat,
                'barang_bukti' => $request->barang_bukti,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TIK  $tIK
     * @return \Illuminate\Http\Response
     */
    public function show(TIK $tIK)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TIK  $tIK
     * @return \Illuminate\Http\Response
     */
    public function edit(TIK $tIK)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TIK  $tIK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TIK $tIK)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TIK  $tIK
     * @return \Illuminate\Http\Response
     */
    public function destroy(TIK $tIK)
    {
        //
    }
}
