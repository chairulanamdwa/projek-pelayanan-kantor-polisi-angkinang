<?php

namespace App\Http\Controllers;

use App\Masyarakat;
use Illuminate\Http\Request;

class AutoCompliteController extends Controller
{
    public function getMasyarakat(Request $request)
    {
        $data = Masyarakat::where('nik', $request->data)->first();
        echo json_encode($data);
    }
}
