<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PelayananUmum extends Controller
{
    public function index()
    {
        $data['title'] = 'Pelayanan Umum';
        return view('pages.pelayanan-umum', $data);
    }
}
