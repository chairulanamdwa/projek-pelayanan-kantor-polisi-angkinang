<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class PetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Data Petugas';
        $data['sidebar'] = 'petugas';
        $data['petugas'] = User::orderBy('name', 'ASC')->where('role', 'petugas')->paginate(20);
        return view('master.petugas.index', $data);
    }

    public function cari(Request $request)
    {
        $nilai = $request->data;
        $data['title'] = 'Data Petugas';
        $data['petugas'] = User::where('role', '==', 'petugas')
            // ->orWhere('name', 'LIKE', '%' . $nilai . '%')
            // ->orWhere('nik', 'LIKE', '%' . $nilai . '%')
            ->orderBy('name', 'ASC')->get();
        return view('master.petugas.cari', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Tambah Petugas';
        $data['sidebar'] = 'petugas';
        return view('master.petugas.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'nik' => 'required',
            'nip' => 'required',
            'jk' => 'required',
            'tmpt_lhr' => 'required',
            'tgl_lhr' =>  'required',
            'pangkat' => 'required',
            'role' => 'required',
            'alamat' => 'required',
            'email' => ['required', 'unique:users'],
        ]);

        User::create([
            'name' => $request->name,
            'nik' => $request->nik,
            'nip' => $request->nip,
            'jk' => $request->jk,
            'tmpt_lhr' => $request->tmpt_lhr,
            'tgl_lhr' =>  $request->tgl_lhr,
            'pangkat' => $request->pangkat,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->nik),
        ]);

        return redirect()->back()->with('message', 'Petugas dengan name ' . $request->name . ' berhasil di tambahkan. Password adalah NIK');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Petugas  $petugas
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Petugas  $petugas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Edit Petugas';
        $data['sidebar'] = 'petugas';
        $data['petugas'] = User::where('id', $id)->first();
        return view('master.petugas.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Petugas  $petugas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'nik' => 'required',
            'nip' => 'required',
            'jk' => 'required',
            'tmpt_lhr' => 'required',
            'tgl_lhr' => 'required',
            'pangkat' => 'required',
            'alamat' => 'required',
            'role' => 'required',
        ]);

        User::where('id', $id)->update([
            'name' => $request->name,
            'nik' => $request->nik,
            'nip' => $request->nip,
            'jk' => $request->jk,
            'tmpt_lhr' => $request->tmpt_lhr,
            'tgl_lhr' => $request->tgl_lhr,
            'pangkat' => $request->pangkat,
            'alamat' => $request->alamat,
            'role' => $request->role,
        ]);

        return redirect()->back()->with('message', 'Petugas data dengan name ' . $request->name . ' berhasil di ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Petugas  $petugas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        User::where('id', $request->data)->delete();
        return "Data Berhasil di Hapus";
    }
}
