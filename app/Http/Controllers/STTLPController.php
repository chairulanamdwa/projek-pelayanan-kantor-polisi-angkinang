<?php

namespace App\Http\Controllers;

use App\STTLP;
use WordTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class STTLPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'STTLP';
        $data['sidebar'] = 'pelayanan';
        return view('pages.sttlp.index', $data);
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelapor' => 'required',
            'curat' => 'required',
            'barang_bukti' => 'required',
        ]);

        STTLP::create([
            'id_pelapor' => $request->id_pelapor,
            'id_user' => Auth::user()->id,
            'data' => json_encode([
                'curat' => $request->curat,
                'barang_bukti' => $request->barang_bukti,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\STTLP  $sTTLP
     * @return \Illuminate\Http\Response
     */
    public function show(STTLP $sTTLP)
    {
        $data['title'] = 'STTLP';
        $data['sidebar'] = 'laporan';
        $data['STTLP_proses'] = STTLP::where('status', 'proses')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sttlp.id_pelapor')
            ->get();
        $data['STTLP_diselidiki'] = STTLP::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sttlp.id_pelapor')
            ->join('users', 'users.id', 'tb_sttlp.id_petugas')
            ->get();
        $data['STTLP_selesai'] = STTLP::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sttlp.id_pelapor')
            ->join('users', 'users.id', 'tb_sttlp.id_petugas')
            ->get();

        return view('pages.sttlp.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\STTLP  $sTTLP
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'STTLP';
        $data['sidebar'] = 'STTLP';
        $data['id'] = $id;
        return view('pages.sttlp.cek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\STTLP  $sTTLP
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        STTLP::where('id_sttlp', $id)->update([
            'status' => 'selesai'
        ]);
        return redirect()->back()->with('message', 'STTLP dengan ID ' . $id . ' Telah selesai diselidiki');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\STTLP  $sTTLP
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        STTLP::where('id_sttlp', $id)->delete();
        return redirect()->back()->with('message', 'STTLP ID ' . $id . ' Telah dihapus');
    }

    public function search(Request $request)
    {
        $from = $request->date1;
        $to = $request->date2;
        $data['laporan'] = STTLP::whereBetween('created_at', [$from, $to])->get();
        return view('pages.sttlp.search', $data);
    }

    public function cetak(Request $request, $id)
    {

        $request->validate([
            'no_surat' => 'required',
        ]);

        STTLP::where('id_sttlp', $id)->update([
            'status' => 'diselidiki',
            'id_petugas' => Auth::user()->id,
        ]);

        $data = STTLP::where('id_sttlp', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sttlp.id_pelapor')
            ->first();


        $namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $hari = date("D");

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        $file = public_path('assets/format-surat/sttlp.rtf');
        $array = array(
            '[tahun]' => date('Y'),
            '[no_surat]' => $request->no_surat,
            '[data_nama]' => $data->nama,
            '[data_ttl]' => $data->tmpt_lhr . ',' . date('d-m-Y', strtotime($data->tgl_lhr)),
            '[data_agama]' => $data->agama,
            '[data_pekerjaan]' => $data->pekerjaan,
            '[data_jk]' => $data->jk,
            '[data_alamat]' => $data->alamat,
            '[data_telpon]' => $data->telpon,
            '[data_curat]' => json_decode($data->data, true)['curat'],
            '[data_barang_bukti]' => json_decode($data->data, true)['barang_bukti'],

            '[data_waktu]' => date('H.i'),
            '[data_hari]' => $hari_ini,
            '[data_tanggal]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'sttlp-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);
    }

    public function printSelidiki()
    {
        $data['title'] = 'STTLP yang di Selidiki';
        $data['STTLP_diselidiki'] = STTLP::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sttlp.id_pelapor')
            ->join('users', 'users.id', 'tb_sttlp.id_petugas')
            ->get();
        return view('pages.sttlp.print_selidiki', $data);
    }

    public function printSelesai()
    {
        $data['title'] = 'STTLP yang sudah Selesai';
        $data['STTLP_selesai'] = STTLP::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sttlp.id_pelapor')
            ->join('users', 'users.id', 'tb_sttlp.id_petugas')
            ->get();
        return view('pages.sttlp.print_selesai', $data);
    }
}
