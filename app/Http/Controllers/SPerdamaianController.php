<?php

namespace App\Http\Controllers;

use App\SPerdamaian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use WordTemplate;

class SPerdamaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Surat Perdamaian';
        $data['sidebar'] = 'pelayanan';
        return view('pages.surat-perdamaian.index', $data);
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelapor' => 'required',
            'id_pelapor2' => 'required',
            'tgl_kejadian' => 'required',
            'tentang' => 'required',
        ]);

        SPerdamaian::create([
            'id_pelapor' => $request->id_pelapor,
            'id_pelapor2' => $request->id_pelapor2,
            'id_user' => Auth::user()->id,
            'data' => json_encode([
                'tgl_kejadian' => $request->tgl_kejadian,
                'tentang' => $request->tentang,
            ]),
            'status' => 'proses',
        ]);

        return redirect()->back()->with('message', 'Data Berhasil di Buat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SPerdamaian  $sPerdamaian
     * @return \Illuminate\Http\Response
     */
    public function show(SPerdamaian $sPerdamaian)
    {
        $data['title'] = 'Surat Perdamaian';
        $data['sidebar'] = 'laporan';
        $data['SPerdamaian_proses'] = SPerdamaian::where('status', 'proses')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sperdamaian.id_pelapor')
            ->get();
        $data['SPerdamaian_diselidiki'] = SPerdamaian::where('status', 'diselidiki')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sperdamaian.id_pelapor')
            ->join('users', 'users.id', 'tb_sperdamaian.id_petugas')
            ->get();
        $data['SPerdamaian_selesai'] = SPerdamaian::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sperdamaian.id_pelapor')
            ->join('users', 'users.id', 'tb_sperdamaian.id_petugas')
            ->get();

        return view('pages.surat-perdamaian.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SPerdamaian  $sPerdamaian
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Surat Perdamaian';
        $data['sidebar'] = 'Surat Perdamaian';
        $data['id'] = $id;
        return view('pages.surat-perdamaian.cek', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SPerdamaian  $sPerdamaian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        SPerdamaian::where('id_sperdamaian', $id)->update([
            'status' => 'selesai'
        ]);
        return redirect()->back()->with('message', 'Surat Perdamaian dengan ID ' . $id . ' Telah selesai diselidiki');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SPerdamaian  $sPerdamaian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SPerdamaian::where('id_sperdamaian', $id)->delete();
        return redirect()->back()->with('message', 'Surat Perdamaian ID ' . $id . ' Telah dihapus');
    }

    public function cetak(Request $request, $id)
    {

        SPerdamaian::where('id_sperdamaian', $id)->update([
            'status' => 'selesai',
            'id_petugas' => Auth::user()->id,
        ]);

        $data = SPerdamaian::where('id_sperdamaian', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sperdamaian.id_pelapor')
            ->first();

        $pertama = SPerdamaian::where('id_sperdamaian', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sperdamaian.id_pelapor')
            ->first();

        $kedua = SPerdamaian::where('id_sperdamaian', $id)
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sperdamaian.id_pelapor2')
            ->first();



        $namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $hari = date('D', strtotime(json_decode($data->data, true)['tgl_kejadian']));

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        $file = public_path('assets/format-surat/surat-perdamaian.rtf');
        $array = array(
            '[data_tahun]' => date('Y'),

            '[nama_pertama]' => $pertama->nama,
            '[ttl_pertama]' => $pertama->tmpt_lhr . ',' . date('d-m-Y', strtotime($pertama->tgl_lhr)),
            '[pekerjaan_pertama]' => $pertama->pekerjaan,
            '[alamat_pertama]' => $pertama->alamat,
            '[telpon_pertama]' => $pertama->telpon,

            '[nama_kedua]' => $kedua->nama,
            '[ttl_kedua]' => $kedua->tmpt_lhr . ',' . date('d-m-Y', strtotime($kedua->tgl_lhr)),
            '[pekerjaan_kedua]' => $kedua->pekerjaan,
            '[alamat_kedua]' => $kedua->alamat,
            '[telpon_kedua]' => $kedua->telpon,
            '[data_tentang]' => json_decode($data->data, true)['tentang'],

            '[data_hari]' => $hari_ini,
            '[data_tgl]' => date('d-m-Y', strtotime(json_decode($data->data, true)['tgl_kejadian'])),
            '[tgl_surat]' => date('d') . '-' . $namaBulan[date('m') - 1] . '-' . date('Y'),
        );

        $nama_file = 'Surat-Perdamaian-' . rand() . '.doc';
        WordTemplate::export($file, $array, $nama_file);
    }


    public function printSelesai()
    {
        $data['title'] = 'Surat Perdamaian yang sudah Selesai';
        $data['SPerdamaian_selesai'] = SPerdamaian::where('status', 'selesai')
            ->join('tb_masyarakat', 'tb_masyarakat.id_masyarakat', 'tb_sperdamaian.id_pelapor')
            ->join('users', 'users.id', 'tb_sperdamaian.id_petugas')
            ->get();
        return view('pages.surat-perdamaian.print_selesai', $data);
    }
}
