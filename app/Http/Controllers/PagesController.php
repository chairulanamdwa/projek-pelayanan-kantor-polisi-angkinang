<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function dashboard()
    {
        $data['title'] = 'Home';
        $data['sidebar'] = 'home';
        return view('pages.dashboard', $data);
    }

    public function pelayananUmum()
    {
        $data['title'] = 'Pelayanan Umum';
        $data['sidebar'] = 'pelayanan';
        return view('pages.pelayanan-umum', $data);
    }

    public function layananMasuk()
    {
        $data['title'] = 'Laporan Bulanan';
        $data['sidebar'] = 'laporan';
        return view('pages.laporan-dan-layananMasuk', $data);
    }
}
