<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class STTLP extends Model
{
    protected $table = 'tb_sttlp';
    protected $fillable = ['id_pelapor', 'data', 'id_user', 'id_petugas', 'status'];
    protected $casts = [
        'data' => 'array'
    ];
}
