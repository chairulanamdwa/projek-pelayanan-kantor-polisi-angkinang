<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SKCK extends Model
{
    protected $table = 'tb_skck';
    protected $fillable = ['id_pelapor', 'data', 'id_user', 'id_petugas', 'status'];
    protected $casts = [
        'data' => 'array'
    ];
}
