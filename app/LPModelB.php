<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LPModelB extends Model
{
    protected $table = 'tb_lp_model_b';
    protected $fillable = ['id_pelapor', 'hari_kejadian', 'tgl_kejadian', 'pukul_kejadian', 'tmpt_kejadian', 'apa_yg_terjadi', 'hari_lapor', 'tgl_lapor', 'pukul_lapor', 'uraian', 'barang_bukti', 'id_petugas', 'id_user', 'status', 'data'];

    protected $casts = [
        'data' => 'array'
    ];
}
