<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SPerdamaian extends Model
{
    protected $table = 'tb_sperdamaian';
    protected $fillable = ['id_pelapor', 'id_pelapor2', 'data', 'id_user', 'id_petugas', 'status'];
    protected $casts = [
        'data' => 'array'
    ];
}
