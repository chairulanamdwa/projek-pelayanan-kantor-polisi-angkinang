<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masyarakat extends Model
{
    protected $table = 'tb_masyarakat';
    protected $fillable = ['nik', 'kk', 'nama', 'tmpt_lhr', 'tgl_lhr', 'jk', 'agama', 'pekerjaan', 'telpon', 'gol_darah', 'pddk_terakhir', 'stts_kawin', 'alamat', 'rt', 'rw'];
}
