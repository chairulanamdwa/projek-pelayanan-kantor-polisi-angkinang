<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengaduanMasyarakat extends Model
{
    protected $table = 'tb_pengaduan_masyarakat';
    protected $fillable = ['id_pelapor', 'data', 'id_user', 'id_petugas', 'status'];
    protected $casts = [
        'data' => 'array'
    ];
}
