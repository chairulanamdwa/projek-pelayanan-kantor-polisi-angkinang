<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $table = 'tb_petugas';
    protected $fillable = ['nama', 'nik', 'nip', 'jk', 'tmpt_lhr', 'tgl_lhr', 'pangkat', 'alamat'];
}
