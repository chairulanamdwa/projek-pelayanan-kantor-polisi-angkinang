@extends('layouts.auth')
@section('title','Login')
@section('body','bg-danger')
@section('content')

<div class="row justify-content-center mt-3">
    <div class="col-lg-4">
        <div class="card shadow-lg border-0">
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-12 text-center mb-2">
                        <img src="{{url('public/assets/img/Lambang_Polri.png')}}" class="rounded-circle" width="100px;"
                            height="100px">
                    </div>
                    <div class="col-12">
                        <h4 class="text-center "> <span class="font-weight-bold">Login ke Aplikasi</span>
                            POLSEK ANGKINANG
                        </h4>
                    </div>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                                placeholder="Masukkan Email Admin">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="current-password" placeholder="Password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    Ingat Saya
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-warning btn-block">
                                Masuk Aplikasi
                            </button>
                        </div>
                    </div>

                    <p class="mt-4">Belum punya akun? <a href="{{route('register')}}">Klik disini</a></p>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection