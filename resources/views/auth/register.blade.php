@extends('layouts.auth')
@section('title','Login')
@section('body','bg-danger')
@section('content')
<div class="row justify-content-center mt-3">
    <div class="col-lg-4">
        <div class="card shadow border-0">
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-12 text-center mb-2">
                        <img src="{{url('public/assets/img/Lambang_Polri.png')}}" class="rounded-circle" width="100px;"
                            height="100px">
                    </div>
                    <div class="col-12">
                        <h4 class="text-center "> <span class="font-weight-bold">Login ke Aplikasi</span>
                            POLSEK ANGKINANG
                        </h4>
                    </div>
                </div>
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ old('name') }}" required autocomplete="name"
                                placeholder="Nama Lengkap" autofocus>
                            @error('name')
                            <span class="invalid-feedback text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" placeholder="Alamat Email" required
                                autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="new-password" placeholder="Password">
                            @error('password')
                            <span class="invalid-feedback text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation" required autocomplete="new-password"
                                placeholder="Konfirmasi Password">
                        </div>
                    </div>

                    <div class="form-group ">
                        <button type="submit" class="btn btn-warning btn-block">
                            Registrasi
                        </button>
                    </div>
                    <p>Sudah punya akun ? <a href="{{route('login')}}">Klik disini</a></p>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection