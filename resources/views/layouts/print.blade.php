<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Laporan Layanan {{$title}}</title>

    <!-- Custom styles for this template-->
    <link href="{{url('public/assets')}}/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body>

    <div class="row">
        <div class="col-12 text-center">
            <img src="{{url('public/assets/img/Lambang_Polri.png')}}" class="img-fluid rounded" style="width: 80px">
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <h2>Kantor Polisi Angkinang Hulu Sungai Selatan</h2>
            <h4>Report Layanan {{$title}}</h4>
        </div>
    </div>
    @yield('content')


    <script>
        window.print();
    </script>


</body>

</html>