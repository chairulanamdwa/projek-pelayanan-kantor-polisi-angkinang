@include('layouts.includes.header')

<!-- Page Wrapper -->
<div id="wrapper" class="mt-5">
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column mt-5 bg-transparent">

        <!-- Main Content -->
        <div id="content">

            <!-- Begin Page Content -->
            <div class="container-fluid">

                @yield('content')
            </div>
            <!-- /.container-fluid -->



        </div>
        <!-- End of Main Content -->