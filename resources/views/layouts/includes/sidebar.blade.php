<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <img src="{{url('public/assets/img/Lambang_Polri.png')}}" class="img-fluid rounded">
        </div>
        <div class="sidebar-brand-text mx-3">Polsek Angkinang</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Heading -->
    <div class="sidebar-heading mt-2">
        Navigasi
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    @if ($sidebar == 'home')
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="fas fa-tachometer-alt fa-fw"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    @if ($sidebar == 'pelayanan')
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{route('pelayanan-umum')}}">
            <i class="fas fa-fw fa-hand-holding-heart"></i>
            <span>Pelayanan Umum</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    @if (Auth::user()->role != 'user')
    @if ($sidebar == 'laporan')
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{route('laporanLayanan')}}">
            <i class="fas fa-fw fa-file-archive"></i>
            <span> Layanan Masuk & Laporan</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    @if ($sidebar == 'petugas')
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{route('petugas')}}">
            <i class="fas fa-fw fa-user-tie"></i>
            <span>Petugas</span>
        </a>
    </li>
    @if ($sidebar == 'Masyarakat')
    <li class="nav-item active">
        @else
    <li class="nav-item">
        @endif
        <a class="nav-link" href="{{route('masyarakat')}}">
            <i class="fas fa-fw fa-database"></i>
            <span>Data Masyarakat</span>
        </a>
    </li>
    @endif
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

</ul>
<!-- End of Sidebar -->