@extends('layouts.app')

@section('title','Pengaduan Masyarakat')
@section('content')


{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('pelayanan-umum')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali ke menu</a>
@if (Auth::user()->role != 'user')
<a href="{{route('PMasyarakatShow')}}" class="badge"><i class="fas fa-file-archive"></i> Arsip Data</a>
@endif
<br>
<br>

<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active text-danger" id="nav-pelapor-tab" data-toggle="tab" href="#nav-pelapor"
            role="tab" aria-controls="nav-pelapor" aria-selected="true">Pelapor</a>
        <a class="nav-item nav-link text-danger" id="nav-terlapor-tab" data-toggle="tab" href="#nav-terlapor" role="tab"
            aria-controls="nav-terlapor" aria-selected="false">Terlapor</a>
        <a class="nav-item nav-link text-danger" id="nav-peristiwa-tab" data-toggle="tab" href="#nav-peristiwa"
            role="tab" aria-controls="nav-peristiwa" aria-selected="false">Peristiwa</a>
        <a class="nav-item nav-link text-danger" id="nav-saksi-tab" data-toggle="tab" href="#nav-saksi" role="tab"
            aria-controls="nav-saksi" aria-selected="false">Saksi</a>
        <a class="nav-item nav-link text-danger" id="nav-uraian-tab" data-toggle="tab" href="#nav-uraian" role="tab"
            aria-controls="nav-uraian" aria-selected="false">Uraian</a>
    </div>
</nav>
<form action="{{route('create-PMasyarakat')}}" method="POST">
    @csrf
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-pelapor" role="tabpanel" aria-labelledby="nav-pelapor-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nik_pelapor">NIK</label>
                        <input type="hidden" name="id_pelapor" id="id_pelapor" class="form-control form-control-sm ">
                        <input type="text" name="nik_pelapor" id="nik_pelapor" class="form-control form-control-sm ">
                        @if ($errors->has('id_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('id_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nama_pelapor">Nama Lengkap</label>
                        <input type="text" name="nama_pelapor" id="nama_pelapor" class="form-control form-control-sm">
                        @if ($errors->has('nama_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tmpt_lhr_pelapor">Tempat Lahir</label>
                                <input type="text" name="tmpt_lhr_pelapor" id="tmpt_lhr_pelapor"
                                    class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tgl_lhr_pelapor">Tanggal Lahir</label>
                                <input type="date" name="tgl_lhr_pelapor" id="tgl_lhr_pelapor"
                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jk_pelapor">Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_pelapor" id="jk_laki_pelapor"
                                value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_pelapor" id="jk_perempun_pelapor"
                                value="P">Perempuan</label>
                        @if ($errors->has('jk_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_pelapor">Pekerjaan</label>
                        <input type="text" name="pekerjaan_pelapor" id="pekerjaan_pelapor"
                            class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="alamat_pelapor">Alamat</label>
                        <textarea type="text" name="alamat_pelapor" id="alamat_pelapor"
                            class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telpon_pelapor">No Telp/FAX/Email</label>
                        <input type="text" name="telpon_pelapor" id="telpon_pelapor"
                            class="form-control form-control-sm">
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-terlapor" role="tabpanel" aria-labelledby="nav-terlapor-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nik_terlapor">NIK</label>
                        <input type="text" name="nik_terlapor" id="nik_terlapor" class="form-control form-control-sm ">
                        @if ($errors->has('nik_terlapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nik_terlapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nama_terlapor">Nama Lengkap</label>
                        <input type="text" name="nama_terlapor" id="nama_terlapor" class="form-control form-control-sm">
                        @if ($errors->has('nama_terlapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_terlapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tmpt_lhr_terlapor">Tempat Lahir</label>
                                <input type="text" name="tmpt_lhr_terlapor" id="tmpt_lhr_terlapor"
                                    class="form-control form-control-sm">
                                @if ($errors->has('tmpt_lhr_terlapor'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tmpt_lhr_terlapor') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tgl_lhr_terlapor">Tanggal Lahir</label>
                                <input type="date" name="tgl_lhr_terlapor" id="tgl_lhr_terlapor"
                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                                @if ($errors->has('tgl_lhr_terlapor'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tgl_lhr_terlapor') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jk_terlapor">Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_terlapor" id="jk_laki_terlapor"
                                value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_terlapor" id="jk_perempun_terlapor"
                                value="P">Perempuan</label>
                        @if ($errors->has('jk_terlapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_terlapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_terlapor">Pekerjaan</label>
                        <input type="text" name="pekerjaan_terlapor" id="pekerjaan_terlapor"
                            class="form-control form-control-sm">
                        @if ($errors->has('pekerjaan_terlapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan_terlapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat_terlapor">Alamat</label>
                        <textarea type="text" name="alamat_terlapor" id="alamat_terlapor"
                            class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_terlapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_terlapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telpon_terlapor">No Telp/FAX/Email</label>
                        <input type="text" name="telpon_terlapor" id="telpon_terlapor"
                            class="form-control form-control-sm">
                        @if ($errors->has('telpon_terlapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('telpon_terlapor') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-peristiwa" role="tabpanel" aria-labelledby="nav-peristiwa-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="waktu_kejadian">Waktu Kejadian</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" name="hari_kejadian" id="hari_kejadian" placeholder="Hari"
                                    class="form-control form-control-sm">
                            </div>
                            <div class="col">
                                <input type="date" name="tgl_kejadian" id="tgl_kejadian" placeholder="Hari"
                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="col">
                                <input type="time" name="pukul_kejadian" id="pukul_kejadian" placeholder="Hari"
                                    class="form-control form-control-sm">
                            </div>
                        </div>
                        @if ($errors->has('hari_kejadian'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('hari_kejadian') }}</strong>
                        </span>
                        @endif
                        @if ($errors->has('tgl_kejadian'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_kejadian') }}</strong>
                        </span>
                        @endif
                        @if ($errors->has('pukul_kejadian'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pukul_kejadian') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tmpt_kejadian">Tempat Kejadian</label>
                        <input type="text" name="tmpt_kejadian" id="tmpt_kejadian" class="form-control form-control-sm">
                        @if ($errors->has('tmpt_kejadian'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tmpt_kejadian') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="apa_yg_terjadi">Apa Yang Terjadi</label>
                        <input type="text" name="apa_yg_terjadi" id="apa_yg_terjadi"
                            class="form-control form-control-sm">
                        @if ($errors->has('apakah_yang_terjadi'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('apakah_yang_terjadi') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="waktu_kejadian">Waktu Lapor</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" name="hari_lapor" id="hari_lapor" placeholder="Hari"
                                    class="form-control form-control-sm">
                            </div>
                            <div class="col">
                                <input type="date" name="tgl_lapor" id="tanggal_lapor" placeholder="Hari"
                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="col">
                                <input type="time" name="pukul_lapor" id="pukul_lapor" placeholder="Hari"
                                    class="form-control form-control-sm">
                            </div>
                        </div>
                        @if ($errors->has('hari_lapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('hari_lapor') }}</strong>
                        </span>
                        @endif
                        @if ($errors->has('tgl_lapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_lapor') }}</strong>
                        </span>
                        @endif
                        @if ($errors->has('pukul_lapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pukul_lapor') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-saksi" role="tabpanel" aria-labelledby="nav-saksi-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nik_1">#1 NIK </label>
                        <input type="text" name="nik_1" id="nik_1" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="nama_1">#1 Nama </label>
                        <input type="text" name="nama_1" id="nama_1" class="form-control form-control-sm">
                        @if ($errors->has('nama_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tmpt_lhr_1">#1 TTL </label>
                        <div class="row">
                            <div class="col-6">
                                <input type="text" name="tmpt_lhr_1" id="tmpt_lhr_1"
                                    class="form-control form-control-sm">
                            </div>
                            <div class="col-6">
                                <input type="date" name="tgl_lhr_1" id="tgl_lhr_1" class="form-control form-control-sm">
                            </div>
                        </div>
                        @if ($errors->has('tmpt_lhr_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tmpt_lhr_1') }}</strong>
                        </span>
                        @endif
                        @if ($errors->has('tgl_lhr_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_lhr_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="jk_1">#1 Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_1" id="jk_laki_1" value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_1" id="jk_perempun_1"
                                value="P">Perempuan</label>
                        @if ($errors->has('jk_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_1">#1 Pekerjaan </label>
                        <input type="text" name="pekerjaan_1" id="pekerjaan_1" class="form-control form-control-sm">
                        @if ($errors->has('pekerjaan_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat_1">#1 Alamat </label>
                        <textarea name="alamat_1" id="alamat_1" class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="agama_1">#1 Agama </label>
                        <input type="text" name="agama_1" id="agama_1" class="form-control form-control-sm">
                        @if ($errors->has('agama_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('agama_1') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-uraian" role="tabpanel" aria-labelledby="nav-uraian-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <textarea type="text" name="uraian" id="uraian" class="form-control form-control-sm" rows="6"
                            placeholder="Uraian"></textarea>
                        @if ($errors->has('uraian'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('uraian') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
    <br>
    <button class="btn btn-warning btn-block mt-2">Masukkan Data dan Buat Surat</button>
</form>
@endsection

@section('js')
<script>
    $('#nik_pelapor').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_pelapor').val('');
        $('#nama_pelapor').val('');
        $('#tmpt_lhr_pelapor').val('');
        $('#pekerjaan_pelapor').val('')
        $('#alamat_pelapor').val('')
        $('#tgl_lhr_pelapor').val('');
        $('#telpon_pelapor').val('')
        $('#jk_laki_pelapor').attr('checked', false);
        $('#jk_perempun_pelapor').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik_pelapor').addClass('is-invalid')
            }else{
                $('#nik_pelapor').removeClass('is-invalid')
                $('#nik_pelapor').addClass('is-valid')
                $('#id_pelapor').val(json.id_masyarakat)
                $('#nama_pelapor').val(json.nama)
                $('#tmpt_lhr_pelapor').val(json.tmpt_lhr)
                $('#tgl_lhr_pelapor').val(json.tgl_lhr)
                $('#pekerjaan_pelapor').val(json.pekerjaan)
                $('#alamat_pelapor').val(json.alamat)
                $('#telpon_pelapor').val(json.telpon)
                if (json.jk == 'L') {
                    $('#jk_laki_pelapor').attr('checked', 'checked');
                }else{
                    $('#jk_perempun_pelapor').attr('checked', 'checked');
                }
            }
        }
        })
        });
    $('#nik_terlapor').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_terlapor').val('');
        $('#nama_terlapor').val('');
        $('#tmpt_lhr_terlapor').val('');
        $('#pekerjaan_terlapor').val('')
        $('#alamat_terlapor').val('')
        $('#tgl_lhr_terlapor').val('');
        $('#telpon_terlapor').val('')
        $('#jk_laki_terlapor').attr('checked', false);
        $('#jk_perempun_terlapor').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik_terlapor').addClass('is-invalid')
            }else{
                $('#nik_terlapor').removeClass('is-invalid')
                $('#nik_terlapor').addClass('is-valid')
                $('#id_terlapor').val(json.id_masyarakat)
                $('#nama_terlapor').val(json.nama)
                $('#tmpt_lhr_terlapor').val(json.tmpt_lhr)
                $('#tgl_lhr_terlapor').val(json.tgl_lhr)
                $('#pekerjaan_terlapor').val(json.pekerjaan)
                $('#alamat_terlapor').val(json.alamat)
                $('#telpon_terlapor').val(json.telpon)
                if (json.jk == 'L') {
                    $('#jk_laki_terlapor').attr('checked', 'checked');
                }else{
                    $('#jk_perempun_terlapor').attr('checked', 'checked');
                }
            }
        }
        })
        });
    $('#nik_1').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_1').val('');
        $('#nama_1').val('');
        $('#tmpt_lhr_1').val('')
        $('#tgl_lhr_1').val('')
        $('#pekerjaan_1').val('')
        $('#alamat_1').val('')
        $('#agama_1').val('')
        $('#jk_laki_1').attr('checked', false);
        $('#jk_perempun_1').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik_1').addClass('is-invalid')
            }else{
                $('#nik_1').removeClass('is-invalid')
                $('#nik_1').addClass('is-valid')
                $('#id_1').val(json.id_masyarakat)
                $('#nama_1').val(json.nama)
                $('#tmpt_lhr_1').val(json.tmpt_lhr)
                $('#tgl_lhr_1').val(json.tgl_lhr)
                $('#pekerjaan_1').val(json.pekerjaan)
                $('#alamat_1').val(json.alamat)
                $('#agama_1').val(json.agama)
                if (json.jk == 'L') {
                    $('#jk_laki_1').attr('checked', 'checked');
                }else{
                    $('#jk_perempun_1').attr('checked', 'checked');
                }
            }
        }
        })
        });
  
    
</script>
@endsection