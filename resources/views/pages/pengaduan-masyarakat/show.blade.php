@extends('layouts.app')
@section('title','Layanan Pengaduan Masyarakat')

@section('content')


{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('laporanLayanan')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali</a>
<br><br>

<div class="row">
    <div class="col-md-6 col-12 mt-3">
        <div class="card shadow border-0">
            <div class="card-body">
                <h4>Layanan Masuk</h4>
                <table class="table table-hover table-bordered table-responsive dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th>#</th>
                            <th>Status</th>
                            <th>Aksi <span class="text-danger">*</span></th>
                            <th>Nama Pelapor</th>
                            <th>NIK</th>
                            <th>Jensi Kelamin</th>
                            <th>Alamat</th>
                            <th>Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody id="data-nonajax">
                        @foreach ($PMasyarakat_proses as $item)
                        <tr>
                            <td scope="row">{{$loop->iteration}}</td>
                            <td><span class="badge badge-warning">{{$item->status}}</span></td>
                            <td>
                                <a href="{{route('PMasyarakatCek',$item->id_pengaduan_masyarakat)}}"
                                    class="btn btn-primary btn-sm" data-id="{{$item->id_pengaduan_masyarakat}}"><i
                                        class="fas fa-print fa-fw "></i> Cetak
                                    Surat</a>
                            </td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->nik}}</td>
                            <td>{{$item->jk}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>
                                <ul>
                                    <li>Hari Kejadian :{{json_decode($item->data,true)['hari_kejadian']}}</li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tbody id="data-ajax"></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12 mt-3">
        <div class="card shadow border-0">
            <div class="card-body">
                <h4>Layanan yang diselidiki</h4>
                <a href="{{route('PMasyarakatPrintSelidiki')}}" class="badge"><i class="fas fa-print"></i> Cetak
                    Laporan</a>
                <br>
                <br>
                <table class="table table-hover table-bordered table-responsive dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th>#</th>
                            <th>Status</th>
                            <th>Aksi <span class="text-danger">*</span></th>
                            <th>Nama Pelapor</th>
                            <th>NIK</th>
                            <th>Jensi Kelamin</th>
                            <th>Alamat</th>
                            <th>Deskripsi</th>
                            <th>Petugas</th>
                        </tr>
                    </thead>
                    <tbody id="data-nonajax">
                        @foreach ($PMasyarakat_diselidiki as $item)
                        <tr>
                            <td scope="row">{{$loop->iteration}}</td>
                            <td><span class="badge badge-info">{{$item->status}}</span></td>
                            <td>
                                <form action="{{route('PMasyarakatSelesai',$item->id_pengaduan_masyarakat)}}"
                                    method="POST">
                                    @method('PATCH')
                                    @csrf
                                    <button class="btn btn-success btn-sm">
                                        <i class="fas fa-check"></i> Selesai
                                    </button>
                                </form>
                            </td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->nik}}</td>
                            <td>{{$item->jk}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>
                                <ul>
                                    <li>Organisasi :{{json_decode($item->data,true)['hari_kejadian']}}</li>
                                </ul>
                            </td>
                            <td>{{$item->name}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tbody id="data-ajax"></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12 mt-3">
        <div class="card shadow border-0">
            <div class="card-body">
                <h4>Layanan yang sudah selesai</h4>
                <a href="{{route('PMasyarakatPrintSelesai')}}" class="badge"><i class="fas fa-print"></i> Cetak
                    Laporan</a>
                <br>
                <br>
                <table class="table table-hover table-bordered table-responsive dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th>#</th>
                            <th>Status</th>
                            <th>Aksi <span class="text-danger">*</span></th>
                            <th>Nama Pelapor</th>
                            <th>NIK</th>
                            <th>Jensi Kelamin</th>
                            <th>Alamat</th>
                            <th>Deskripsi</th>
                            <th>Petugas</th>
                        </tr>
                    </thead>
                    <tbody id="data-nonajax">
                        @foreach ($PMasyarakat_selesai as $item)
                        <tr>
                            <td scope="row">{{$loop->iteration}}</td>
                            <td><span class="badge badge-success">{{$item->status}}</span></td>
                            <td>
                                <form action="{{route('PMasyarakatDestroy',$item->id_pengaduan_masyarakat)}}"
                                    method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')">
                                        <i class="fas fa-times"></i> Hapus
                                    </button>
                                </form>
                            </td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->nik}}</td>
                            <td>{{$item->jk}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>
                                <ul>
                                    <li>Organisasi :{{json_decode($item->data,true)['hari_kejadian']}}</li>
                                </ul>
                            </td>
                            <td>{{$item->name}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tbody id="data-ajax"></tbody>
                </table>
            </div>
        </div>
    </div>

</div>


@endsection