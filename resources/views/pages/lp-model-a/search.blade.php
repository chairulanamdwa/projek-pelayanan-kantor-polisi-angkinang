@forelse ($laporan as $item)
<tr>
    <td scope="row">{{$loop->iteration}}</td>
    <td>
        <a href="#" class="hapus-data" data-id="{{$item->id_lp_model_b}}"><i
                class="fas fa-times fa-fw text-danger"></i></a>
    </td>
    <td>{{$item->nama_pelapor}}</td>
    <td>{{$item->nik_pelapor}}</td>
    <td>{{$item->jk_pelapor}}</td>
    <td>{{$item->alamat_pelapor}}</td>
    <td>{{$item->waktu_kejadian}}</td>
    <td>{{$item->tempat_kejadian}}</td>
    <td>{{$item->apa_yang_terjadi}}</td>
    <td>{{$item->nama_terlapor}}</td>
    <td>{{$item->nik_terlapor}}</td>
</tr>
@empty
<tr class="text-center">
    <td colspan="11">Data Masih Kosong</td>
</tr>
@endforelse