@extends('layouts.print')

@section('content')
<table class="table  table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr class="text-center">
            <th>#</th>
            <th>Status</th>
            <th>Nama Pelapor</th>
            <th>NIK</th>
            <th>Jensi Kelamin</th>
            <th>Alamat</th>
            <th>Waktu Kejadian</th>
            <th>Tempat Kejadian</th>
            <th>Apa yg Terjadi</th>
            <th>Petugas</th>
        </tr>
    </thead>
    <tbody id="data-nonajax">
        @foreach ($LPMA_selesai as $item)
        <tr>
            <td scope="row">{{$loop->iteration}}</td>
            <td><span class="badge badge-success">{{$item->status}}</span></td>
            <td>{{$item->nama}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->jk}}</td>
            <td>{{$item->alamat}}</td>
            <td>
                <ul>
                    <li>Hari :{{$item->hari_kejadian}}</li>
                    <li>Tanggal :{{date('d-m-Y',strtotime($item->tgl_kejadian))}}</li>
                    <li>Pukul :{{$item->pukul_kejadian}} Wita</li>
                </ul>
            </td>
            <td>{{$item->tmpt_kejadian}}</td>
            <td>{{$item->apa_yg_terjadi}}</td>
            <td>{{$item->name}}</td>
        </tr>
        @endforeach
    </tbody>
    <tbody id="data-ajax"></tbody>
</table>
@endsection