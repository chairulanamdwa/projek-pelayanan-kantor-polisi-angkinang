@extends('layouts.app')

@section('title','Kartu TIK')
@section('content')


{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('pelayanan-umum')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali ke menu</a>
@if (Auth::user()->role != 'user')
<a href="{{route('TIKShow')}}" class="badge"><i class="fas fa-file-archive"></i> Arsip Data</a>
@endif
<br>
<br>

<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active text-danger" id="nav-pelapor-tab" data-toggle="tab" href="#nav-pelapor"
            role="tab" aria-controls="nav-pelapor" aria-selected="true">Pelapor</a>
        <a class="nav-item nav-link text-danger" id="nav-ortu-pelapor-tab" data-toggle="tab" href="#nav-ortu-pelapor"
            role="tab" aria-controls="nav-ortu-pelapor" aria-selected="false">Data Ortu Pelapor</a>
        <a class="nav-item nav-link text-danger" id="nav-ortu-pasangan-tab" data-toggle="tab" href="#nav-ortu-pasangan"
            role="tab" aria-controls="nav-ortu-pasangan" aria-selected="false">Data Ortu Pasangan</a>
        <a class="nav-item nav-link text-danger" id="nav-saksi-tab" data-toggle="tab" href="#nav-saksi" role="tab"
            aria-controls="nav-anak" aria-selected="false">Data Anak</a>
        <a class="nav-item nav-link text-danger" id="nav-ciri-badan-tab" data-toggle="tab" href="#nav-ciri-badan"
            role="tab" aria-controls="nav-ciri-badan" aria-selected="false">Ciri-Ciri Badan</a>
    </div>
</nav>
<form action="{{route('create-sik')}}" method="POST">
    @csrf
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-pelapor" role="tabpanel" aria-labelledby="nav-pelapor-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nik_pelapor">NIK</label>
                        <input type="hidden" name="id_pelapor" id="id_pelapor" class="form-control form-control-sm ">
                        <input type="text" name="nik_pelapor" id="nik_pelapor" class="form-control form-control-sm ">
                        @if ($errors->has('id_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('id_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nama_pelapor">Nama Lengkap</label>
                        <input type="text" name="nama_pelapor" id="nama_pelapor" class="form-control form-control-sm">
                        @if ($errors->has('nama_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tmpt_lhr_pelapor">Tempat Lahir</label>
                                <input type="text" name="tmpt_lhr_pelapor" id="tmpt_lhr_pelapor"
                                    class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tgl_lhr_pelapor">Tanggal Lahir</label>
                                <input type="date" name="tgl_lhr_pelapor" id="tgl_lhr_pelapor"
                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jk_pelapor">Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_pelapor" id="jk_laki_pelapor"
                                value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_pelapor" id="jk_perempun_pelapor"
                                value="P">Perempuan</label>
                        @if ($errors->has('jk_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_pelapor">Pekerjaan</label>
                        <input type="text" name="pekerjaan_pelapor" id="pekerjaan_pelapor"
                            class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="alamat_pelapor">Alamat</label>
                        <textarea type="text" name="alamat_pelapor" id="alamat_pelapor"
                            class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telpon_pelapor">No Telp/FAX/Email</label>
                        <input type="text" name="telpon_pelapor" id="telpon_pelapor"
                            class="form-control form-control-sm">
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-ortu-pelapor" role="tabpanel" aria-labelledby="nav-ortu-pelapor-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nik_ortu_pelapor">NIK</label>
                        <input type="text" name="nik_ortu_pelapor" id="nik_ortu_pelapor"
                            class="form-control form-control-sm ">
                        @if ($errors->has('nik_ortu_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nik_ortu_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nama_ortu_pelapor">Nama Lengkap</label>
                        <input type="text" name="nama_ortu_pelapor" id="nama_ortu_pelapor"
                            class="form-control form-control-sm">
                        @if ($errors->has('nama_ortu_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_ortu_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tmpt_lhr_ortu_pelapor">Tempat Lahir</label>
                                <input type="text" name="tmpt_lhr_ortu_pelapor" id="tmpt_lhr_ortu_pelapor"
                                    class="form-control form-control-sm">
                                @if ($errors->has('tmpt_lhr_ortu_pelapor'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tmpt_lhr_ortu_pelapor') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tgl_lhr_ortu_pelapor">Tanggal Lahir</label>
                                <input type="date" name="tgl_lhr_ortu_pelapor" id="tgl_lhr_ortu_pelapor"
                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                                @if ($errors->has('tgl_lhr_ortu_pelapor'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tgl_lhr_ortu_pelapor') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jk_ortu_pelapor">Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_ortu_pelapor" id="jk_laki_ortu_pelapor"
                                value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_ortu_pelapor"
                                id="jk_perempun_ortu_pelapor" value="P">Perempuan</label>
                        @if ($errors->has('jk_ortu_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_ortu_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_ortu_pelapor">Pekerjaan</label>
                        <input type="text" name="pekerjaan_ortu_pelapor" id="pekerjaan_ortu_pelapor"
                            class="form-control form-control-sm">
                        @if ($errors->has('pekerjaan_ortu_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan_ortu_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat_ortu_pelapor">Alamat</label>
                        <textarea type="text" name="alamat_ortu_pelapor" id="alamat_ortu_pelapor"
                            class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_ortu_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_ortu_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telpon_ortu_pelapor">No Telp/FAX/Email</label>
                        <input type="text" name="telpon_ortu_pelapor" id="telpon_ortu_pelapor"
                            class="form-control form-control-sm">
                        @if ($errors->has('telpon_ortu_pelapor'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('telpon_ortu_pelapor') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-ortu-pasangan" role="tabpanel" aria-labelledby="nav-ortu-pasangan-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nik_ortu_pasangan">NIK</label>
                        <input type="text" name="nik_ortu_pasangan" id="nik_ortu_pasangan"
                            class="form-control form-control-sm ">
                        @if ($errors->has('nik_ortu_pasangan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nik_ortu_pasangan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nama_ortu_pasangan">Nama Lengkap</label>
                        <input type="text" name="nama_ortu_pasangan" id="nama_ortu_pasangan"
                            class="form-control form-control-sm">
                        @if ($errors->has('nama_ortu_pasangan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_ortu_pasangan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tmpt_lhr_ortu_pasangan">Tempat Lahir</label>
                                <input type="text" name="tmpt_lhr_ortu_pasangan" id="tmpt_lhr_ortu_pasangan"
                                    class="form-control form-control-sm">
                                @if ($errors->has('tmpt_lhr_ortu_pasangan'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tmpt_lhr_ortu_pasangan') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tgl_lhr_ortu_pasangan">Tanggal Lahir</label>
                                <input type="date" name="tgl_lhr_ortu_pasangan" id="tgl_lhr_ortu_pasangan"
                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                                @if ($errors->has('tgl_lhr_ortu_pasangan'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tgl_lhr_ortu_pasangan') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jk_ortu_pasangan">Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_ortu_pasangan" id="jk_laki_ortu_pasangan"
                                value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_ortu_pasangan"
                                id="jk_perempun_ortu_pasangan" value="P">Perempuan</label>
                        @if ($errors->has('jk_ortu_pasangan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_ortu_pasangan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_ortu_pasangan">Pekerjaan</label>
                        <input type="text" name="pekerjaan_ortu_pasangan" id="pekerjaan_ortu_pasangan"
                            class="form-control form-control-sm">
                        @if ($errors->has('pekerjaan_ortu_pasangan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan_ortu_pasangan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat_ortu_pasangan">Alamat</label>
                        <textarea type="text" name="alamat_ortu_pasangan" id="alamat_ortu_pasangan"
                            class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_ortu_pasangan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_ortu_pasangan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telpon_ortu_pasangan">No Telp/FAX/Email</label>
                        <input type="text" name="telpon_ortu_pasangan" id="telpon_ortu_pasangan"
                            class="form-control form-control-sm">
                        @if ($errors->has('telpon_ortu_pasangan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('telpon_ortu_pasangan') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-anak" role="tabpanel" aria-labelledby="nav-anak-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <label for="nik_1">#1 NIK </label>
                        <input type="text" name="nik_1" id="nik_1" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="nama_1">#1 Nama </label>
                        <input type="text" name="nama_1" id="nama_1" class="form-control form-control-sm">
                        @if ($errors->has('nama_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tmpt_lhr_1">#1 TTL </label>
                        <div class="row">
                            <div class="col-6">
                                <input type="text" name="tmpt_lhr_1" id="tmpt_lhr_1"
                                    class="form-control form-control-sm">
                            </div>
                            <div class="col-6">
                                <input type="date" name="tgl_lhr_1" id="tgl_lhr_1" class="form-control form-control-sm">
                            </div>
                        </div>
                        @if ($errors->has('tmpt_lhr_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tmpt_lhr_1') }}</strong>
                        </span>
                        @endif
                        @if ($errors->has('tgl_lhr_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_lhr_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="jk_1">#1 Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_1" id="jk_laki_1" value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_1" id="jk_perempun_1"
                                value="P">Perempuan</label>
                        @if ($errors->has('jk_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_1">#1 Pekerjaan </label>
                        <input type="text" name="pekerjaan_1" id="pekerjaan_1" class="form-control form-control-sm">
                        @if ($errors->has('pekerjaan_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat_1">#1 Alamat </label>
                        <textarea name="alamat_1" id="alamat_1" class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="agama_1">#1 Agama </label>
                        <input type="text" name="agama_1" id="agama_1" class="form-control form-control-sm">
                        @if ($errors->has('agama_1'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('agama_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="nik_2">#2 NIK </label>
                        <input type="text" name="nik_2" id="nik_2" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="nama_2">#2 Nama </label>
                        <input type="text" name="nama_2" id="nama_2" class="form-control form-control-sm">
                        @if ($errors->has('nama_2'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama_2') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nama_2">#2 TTL </label>
                        <div class="row">
                            <div class="col-6">
                                <input type="text" name="tmpt_lhr_2" id="tmpt_lhr_2"
                                    class="form-control form-control-sm">
                            </div>
                            <div class="col-6">
                                <input type="date" name="tgl_lhr_2" id="tgl_lhr_2" class="form-control form-control-sm">
                            </div>
                        </div>
                        @if ($errors->has('tmpt_lhr_2'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tmpt_lhr_2') }}</strong>
                        </span>
                        @endif
                        @if ($errors->has('tgl_lhr_2'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_lhr_2') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="jk_2">#2 Jenis Kelamin</label><br>
                        <label for="jk_laki"><input type="radio" name="jk_2" id="jk_laki_2" value="L">Laki-Laki</label>
                        <label for="jk_perempun"><input type="radio" name="jk_2" id="jk_perempun_2"
                                value="P">Perempuan</label>
                        @if ($errors->has('jk_2'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('jk_2') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_2">#2 Pekerjaan </label>
                        <input type="text" name="pekerjaan_2" id="pekerjaan_2" class="form-control form-control-sm">
                        @if ($errors->has('pekerjaan_2'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan_2') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat_2">#2 Alamat </label>
                        <textarea name="alamat_2" id="alamat_2" class="form-control form-control-sm"></textarea>
                        @if ($errors->has('alamat_2'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat_2') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="agama_2">#2 Agama </label>
                        <input type="text" name="agama_2" id="agama_2" class="form-control form-control-sm">
                        @if ($errors->has('agama_2'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('agama_2') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-ciri-badan" role="tabpanel" aria-labelledby="nav-ciri-badan-tab">
            <br>
            <div class="card shadow border-0">
                <div class="card-body">
                    <div class="form-group">
                        <textarea type="text" name="uraian" id="uraian" class="form-control form-control-sm" rows="6"
                            placeholder="Uraian"></textarea>
                        @if ($errors->has('uraian'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('uraian') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <textarea type="text" name="barang_bukti" id="barang_bukti" placeholder="Barang Bukti"
                            class="form-control form-control-sm" rows="6"></textarea>
                        @if ($errors->has('barang_bukti'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('barang_bukti') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
    <br>
    <button class="btn btn-warning btn-block mt-2">Masukkan Data dan Buat Surat</button>
</form>
@endsection

@section('js')
<script>
    $('#nik_pelapor').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_pelapor').val('');
        $('#nama_pelapor').val('');
        $('#tmpt_lhr_pelapor').val('');
        $('#pekerjaan_pelapor').val('')
        $('#alamat_pelapor').val('')
        $('#tgl_lhr_pelapor').val('');
        $('#telpon_pelapor').val('')
        $('#jk_laki_pelapor').attr('checked', false);
        $('#jk_perempun_pelapor').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik_pelapor').addClass('is-invalid')
            }else{
                $('#nik_pelapor').removeClass('is-invalid')
                $('#nik_pelapor').addClass('is-valid')
                $('#id_pelapor').val(json.id_masyarakat)
                $('#nama_pelapor').val(json.nama)
                $('#tmpt_lhr_pelapor').val(json.tmpt_lhr)
                $('#tgl_lhr_pelapor').val(json.tgl_lhr)
                $('#pekerjaan_pelapor').val(json.pekerjaan)
                $('#alamat_pelapor').val(json.alamat)
                $('#telpon_pelapor').val(json.telpon)
                if (json.jk == 'L') {
                    $('#jk_laki_pelapor').attr('checked', 'checked');
                }else{
                    $('#jk_perempun_pelapor').attr('checked', 'checked');
                }
            }
        }
        })
        });
    $('#nik_terlapor').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_terlapor').val('');
        $('#nama_terlapor').val('');
        $('#tmpt_lhr_terlapor').val('');
        $('#pekerjaan_terlapor').val('')
        $('#alamat_terlapor').val('')
        $('#tgl_lhr_terlapor').val('');
        $('#telpon_terlapor').val('')
        $('#jk_laki_terlapor').attr('checked', false);
        $('#jk_perempun_terlapor').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik_terlapor').addClass('is-invalid')
            }else{
                $('#nik_terlapor').removeClass('is-invalid')
                $('#nik_terlapor').addClass('is-valid')
                $('#id_terlapor').val(json.id_masyarakat)
                $('#nama_terlapor').val(json.nama)
                $('#tmpt_lhr_terlapor').val(json.tmpt_lhr)
                $('#tgl_lhr_terlapor').val(json.tgl_lhr)
                $('#pekerjaan_terlapor').val(json.pekerjaan)
                $('#alamat_terlapor').val(json.alamat)
                $('#telpon_terlapor').val(json.telpon)
                if (json.jk == 'L') {
                    $('#jk_laki_terlapor').attr('checked', 'checked');
                }else{
                    $('#jk_perempun_terlapor').attr('checked', 'checked');
                }
            }
        }
        })
        });
    $('#nik_1').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_1').val('');
        $('#nama_1').val('');
        $('#tmpt_lhr_1').val('')
        $('#tgl_lhr_1').val('')
        $('#pekerjaan_1').val('')
        $('#alamat_1').val('')
        $('#agama_1').val('')
        $('#jk_laki_1').attr('checked', false);
        $('#jk_perempun_1').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik_1').addClass('is-invalid')
            }else{
                $('#nik_1').removeClass('is-invalid')
                $('#nik_1').addClass('is-valid')
                $('#id_1').val(json.id_masyarakat)
                $('#nama_1').val(json.nama)
                $('#tmpt_lhr_1').val(json.tmpt_lhr)
                $('#tgl_lhr_1').val(json.tgl_lhr)
                $('#pekerjaan_1').val(json.pekerjaan)
                $('#alamat_1').val(json.alamat)
                $('#agama_1').val(json.agama)
                if (json.jk == 'L') {
                    $('#jk_laki_1').attr('checked', 'checked');
                }else{
                    $('#jk_perempun_1').attr('checked', 'checked');
                }
            }
        }
        })
        });
    
    $('#nik_2').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_2').val('');
        $('#nama_2').val('');
        $('#tmpt_lhr_2').val('')
        $('#tgl_lhr_2').val('')
        $('#pekerjaan_2').val('')
        $('#alamat_2').val('')
        $('#agama_2').val('')
        $('#jk_laki_2').attr('checked', false);
        $('#jk_perempun_2').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik_2').addClass('is-invalid')
            }else{
                $('#nik_2').removeClass('is-invalid')
                $('#nik_2').addClass('is-valid')
                $('#id_2').val(json.id_masyarakat)
                $('#nama_2').val(json.nama)
                $('#tmpt_lhr_2').val(json.tmpt_lhr)
                $('#tgl_lhr_2').val(json.tgl_lhr)
                $('#pekerjaan_2').val(json.pekerjaan)
                $('#alamat_2').val(json.alamat)
                $('#agama_2').val(json.agama)
                if (json.jk == 'L') {
                    $('#jk_laki_2').attr('checked', 'checked');
                }else{
                    $('#jk_perempun_2').attr('checked', 'checked');
                }
            }
        }
        })
        });
    
</script>
@endsection