@extends('layouts.app')

@section('title','SKCK')

@section('content')


{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('pelayanan-umum')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali ke menu</a>
@if (Auth::user()->role != 'user')
<a href="{{route('skckShow')}}" class="badge"><i class="fas fa-file-archive"></i> Arsip Data</a>
@endif
<br>
<br>

<div class="row">
    <div class="col-12">
        <form action="{{route('create-skck')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="accordion" id="accordionExample">
                <div class="card shadow border-0">
                    <div class="card-header bg-danger ">
                        <h6 class="mb-0 text-white">
                            Input data SKCK
                        </h6>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="text" name="nik" id="nik" class="form-control form-control-sm">
                                @if ($errors->has('nik'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('nik') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama Lengkap</label>
                                <input type="text" name="nama" id="nama" class="form-control form-control-sm">
                                <input type="hidden" name="id_pelapor" id="id_pelapor"
                                    class="form-control form-control-sm">
                                @if ($errors->has('id_pelapor'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('id_pelapor') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tmpt_lhr">Tempat Lahir</label>
                                        <input type="text" name="tmpt_lhr" id="tmpt_lhr"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('tmpt_lhr'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('tmpt_lhr') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tgl_lhr">Tanggal Lahir</label>
                                        <input type="date" name="tgl_lhr" id="tgl_lhr"
                                            class="form-control form-control-sm tgl_lhr" value="{{date('Y-m-d')}}">
                                        @if ($errors->has('tgl_lhr'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('tgl_lhr') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jk">Jenis Kelamin Terlapor</label><br>
                                <label for="laki_terlapor"><input type="radio" name="jk" id="jk_laki"
                                        value="Laki-Laki">Laki-Laki</label>
                                <label for="perempun_terlapor"><input type="radio" name="jk" id="jk_perempun"
                                        value="Perempuan">Perempuan</label>
                                @if ($errors->has('jk'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('jk') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="agama">Agama</label>
                                <input type="text" name="agama" id="agama" class="form-control form-control-sm">
                                @if ($errors->has('agama'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('agama') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="pekerjaan">Pekerjaan</label>
                                <input type="text" name="pekerjaan" id="pekerjaan" class="form-control form-control-sm">
                                @if ($errors->has('pekerjaan'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('pekerjaan') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="data_sidik_jari_1">Data Sidik Jari 1</label>
                                        <input type="text" name="data_sidik_jari_1" id="data_sidik_jari_1"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('data_sidik_jari_1'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('data_sidik_jari_1') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="data_sidik_jari_2">Data Sidik Jari 2</label>
                                        <input type="text" name="data_sidik_jari_2" id="data_sidik_jari_2"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('data_sidik_jari_2'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('data_sidik_jari_2') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="tanggal_berada_indonesia">Tanggal Berada di Indonesia</label>
                                        <input type="date" name="tanggal_berada_indonesia" id="tanggal_berada_indonesia"
                                            class="form-control form-control-sm tgl_lhr">
                                        @if ($errors->has('tanggal_berada_indonesia'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('tanggal_berada_indonesia') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="sampai">Sampai</label>
                                        <input type="date" name="sampai" id="sampai"
                                            class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                                        @if ($errors->has('sampai'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('sampai') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keperluan">keperluan</label>
                                <input type="text" name="keperluan" id="keperluan" class="form-control form-control-sm">
                                @if ($errors->has('keperluan'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('keperluan') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="gambar">Foto</label>
                                <input type="file" name="gambar" id="gambar" class="form-control form-control-sm">
                                @if ($errors->has('gambar'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('gambar') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="telpon">No Telp/FAX/Email</label>
                                <input type="text" name="telpon" id="telpon" class="form-control form-control-sm">
                                @if ($errors->has('telpon'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('telpon') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-warning btn-block mt-2">Buat Data</button>
        </form>
    </div>
</div>


@endsection


@section('js')
<script>
    $('#nik').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_pelapor').val('');
        $('#nama').val('');
        $('#tmpt_lhr').val('');
        $('#agama').val('')
        $('#pekerjaan').val('')
        $('#alamat').val('')
        $('.tgl_lhr').val('');
        $('#telpon').val('')
        $('#jk_laki').attr('checked', false);
        $('#jk_perempun').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik').addClass('is-invalid')
            }else{
                $('#nik').removeClass('is-invalid')
                $('#nik').addClass('is-valid')
                $('#id_pelapor').val(json.id_masyarakat)
                $('#nama').val(json.nama)
                $('#agama').val(json.agama)
                $('#tmpt_lhr').val(json.tmpt_lhr)
                $('.tgl_lhr').val(json.tgl_lhr)
                $('#pekerjaan').val(json.pekerjaan)
                $('#alamat').val(json.alamat)
                $('#telpon').val(json.telpon)
                if (json.jk == 'L') {
                    $('#jk_laki').attr('checked', 'checked');
                }else{
                    $('#jk_perempun').attr('checked', 'checked');
                }
            }
        }
        })
        });    
</script>
@endsection