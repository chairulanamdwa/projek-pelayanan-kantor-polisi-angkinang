@extends('layouts.app')
@section('title','Layanan SKCK')

@section('content')


{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('laporanLayanan')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali</a>
<br><br>

<div class="row">
    <div class="col-md-6 col-12 mt-3">
        <div class="card shadow border-0">
            <div class="card-body">
                <h4>Layanan Masuk</h4>
                <table class="table table-hover table-bordered table-responsive dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th>#</th>
                            <th>Status</th>
                            <th>Foto</th>
                            <th>Aksi <span class="text-danger">*</span></th>
                            <th>Nama Pelapor</th>
                            <th>NIK</th>
                            <th>Jensi Kelamin</th>
                            <th>Alamat</th>
                            <th>Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody id="data-nonajax">
                        @foreach ($SKCK_proses as $item)
                        <tr>
                            <td scope="row">{{$loop->iteration}}</td>
                            <td><span class="badge badge-warning">{{$item->status}}</span></td>
                            <td>
                                <a href="{{url('public/assets/img/upload').'/'.json_decode($item->data,true)['gambar']}}"
                                    download="{{json_decode($item->data,true)['gambar']}}">
                                    <img src="{{url('public/assets/img/upload').'/'.json_decode($item->data,true)['gambar']}}"
                                        style="width:50px" alt="">
                                </a>
                            </td>
                            <td>
                                <a href="{{route('skckCek',$item->id_skck)}}" class="btn btn-primary btn-sm"
                                    data-id="{{$item->id_skck}}"><i class="fas fa-print fa-fw "></i> Cetak
                                    Surat</a>
                            </td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->nik}}</td>
                            <td>{{$item->jk}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>
                                <ul>
                                    <li>Sidik Jari 1 :{{json_decode($item->data,true)['data_sidik_jari_1']}}</li>
                                    <li>Sidik Jari 2 :{{json_decode($item->data,true)['data_sidik_jari_2']}}</li>
                                    <li>keperluan :{{json_decode($item->data,true)['tanggal_berada_indonesia']}}</li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tbody id="data-ajax"></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12 mt-3">
        <div class="card shadow border-0">
            <div class="card-body">
                <h4>Layanan yang sudah selesai</h4>
                <a href="{{route('skckPrintSelesai')}}" class="badge"><i class="fas fa-print"></i> Cetak Laporan</a>
                <br>
                <br>
                <table class="table table-hover table-bordered table-responsive dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th>#</th>
                            <th>Status</th>
                            <th>Foto</th>
                            <th>Aksi <span class="text-danger">*</span></th>
                            <th>Nama Pelapor</th>
                            <th>NIK</th>
                            <th>Jensi Kelamin</th>
                            <th>Alamat</th>
                            <th>Deskripsi</th>
                            <th>Petugas</th>
                        </tr>
                    </thead>
                    <tbody id="data-nonajax">
                        @foreach ($SKCK_selesai as $item)
                        <tr>
                            <td scope="row">{{$loop->iteration}}</td>
                            <td><span class="badge badge-success">{{$item->status}}</span></td>
                            <td>
                                <a href="{{url('public/assets/img/upload').'/'.json_decode($item->data,true)['gambar']}}"
                                    download="{{json_decode($item->data,true)['gambar']}}">
                                    <img src="{{url('public/assets/img/upload').'/'.json_decode($item->data,true)['gambar']}}"
                                        style="width:50px" alt="">
                                </a>
                            </td>
                            <td>
                                <form action="{{route('skckDestroy',$item->id_skck)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')">
                                        <i class="fas fa-times"></i> Hapus
                                    </button>
                                </form>
                                <a href="{{route('skckCek',$item->id_skck)}}" class="btn btn-warning btn-sm my-1"
                                    data-id="{{$item->id_skck}}"><i class="fas fa-clock fa-fw"></i>
                                    Perpanjang</a>
                            </td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->nik}}</td>
                            <td>{{$item->jk}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>
                                <ul>
                                    <li>Sidik Jari 1 :{{json_decode($item->data,true)['data_sidik_jari_1']}}</li>
                                    <li>Sidik Jari 2 :{{json_decode($item->data,true)['data_sidik_jari_2']}}</li>
                                    <li>keperluan :{{json_decode($item->data,true)['tanggal_berada_indonesia']}}</li>
                                    <li>Berlaku Sampai :{{date('d-m-Y',strtotime('+2 year'.$item->updated_at))}}
                                    </li>
                                </ul>
                            </td>
                            <td>{{$item->name}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tbody id="data-ajax"></tbody>
                </table>
            </div>
        </div>
    </div>

</div>


@endsection