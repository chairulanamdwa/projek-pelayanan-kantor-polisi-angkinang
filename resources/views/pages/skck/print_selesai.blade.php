@extends('layouts.print')

@section('content')
<table class="table table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr class="text-center">
            <th>#</th>
            <th>Status</th>
            <th>Foto</th>
            <th>Nama Pelapor</th>
            <th>NIK</th>
            <th>Jensi Kelamin</th>
            <th>Alamat</th>
            <th>Deskripsi</th>
            <th>Petugas</th>
        </tr>
    </thead>
    <tbody id="data-nonajax">
        @foreach ($SKCK_selesai as $item)
        <tr>
            <td scope="row">{{$loop->iteration}}</td>
            <td><span class="badge badge-success">{{$item->status}}</span></td>
            <td>
                <a href="{{url('public/assets/img/upload').'/'.json_decode($item->data,true)['gambar']}}"
                    download="{{json_decode($item->data,true)['gambar']}}">
                    <img src="{{url('public/assets/img/upload').'/'.json_decode($item->data,true)['gambar']}}"
                        style="width:50px" alt="">
                </a>
            </td>
            <td>{{$item->nama}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->jk}}</td>
            <td>{{$item->alamat}}</td>
            <td>
                <ul>
                    <li>Sidik Jari 1 :{{json_decode($item->data,true)['data_sidik_jari_1']}}</li>
                    <li>Sidik Jari 2 :{{json_decode($item->data,true)['data_sidik_jari_2']}}</li>
                    <li>keperluan :{{json_decode($item->data,true)['tanggal_berada_indonesia']}}</li>
                    <li>Berlaku Sampai :{{date('d-m-Y',strtotime('+2 year'.$item->updated_at))}}
                    </li>
                </ul>
            </td>
            <td>{{$item->name}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection