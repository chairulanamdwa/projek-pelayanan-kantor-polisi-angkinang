@extends('layouts.app')

@section('title','Profil User')
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif


<div class="row my-4">
    <div class="col-lg-4">
        <form action="{{route('profil')}}" method="POST">
            @method('patch')
            @csrf
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control form-control-sm" placeholder="Nama Petugas"
                    value="{{Auth::user()->name}}">
                @if ($errors->has('name'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control form-control-sm" placeholder="Email"
                    value="{{Auth::user()->email}}" readonly>
                @if ($errors->has('email'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="number" name="nik" id="nik" class="form-control form-control-sm" placeholder="NIK"
                    value="{{Auth::user()->nik}}" readonly>
                @if ($errors->has('nik'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('nik') }}</strong>
                    @endif
            </div>
            <div class="form-group">
                <input type="number" name="nip" id="nip" class="form-control form-control-sm" placeholder="NIP"
                    value="{{Auth::user()->nip}}">
                @if ($errors->has('nip'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('nip') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="laki"><input type="radio" name="jk" id="laki" @if (Auth::user()->jk == 'L') checked @endif
                    value="L">Laki-Laki</label>
                <label for="perempuan"><input type="radio" name="jk" id="perempuan" @if (Auth::user()->jk == 'P')
                    checked @endif value="P">Perempuan</label>
                @if ($errors->has('jk'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('jk') }}</strong>
                </span>
                @endif
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <input type="text" name="tmpt_lhr" id="tmpt_lhr" class="form-control form-control-sm"
                            placeholder="Tempat Lahir" value="{{Auth::user()->tmpt_lhr}}">
                        @if ($errors->has('tmpt_lhr'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tmpt_lhr') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input type="date" name="tgl_lhr" id="tgl_lhr" class="form-control form-control-sm"
                            placeholder="Tanggal Lahir" value="{{date('Y-m-d',strtotime(Auth::user()->tgl_lhr))}}">
                        @if ($errors->has('tgl_lhr'))
                        <span class=" text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_lhr') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            @if (Auth::user()->role != 'user')
            <div class="form-group">
                <input type="text" name="pangkat" id="pangkat" class="form-control form-control-sm"
                    placeholder="Pangkat" {{Auth::user()->pangkat}}>
                @if ($errors->has('pangkat'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('pangkat') }}</strong>
                </span>
                @endif
            </div>
            @endif
            <div class="form-group">
                <textarea name="alamat" id="alamat" class="form-control form-control-sm" placeholder="Alamat"
                    rows="4">{{Auth::user()->alamat}}</textarea>
                @if ($errors->has('alamat'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('alamat') }}</strong>
                </span>
                @endif
            </div>
            <button class="btn btn-dark btn-sm btn-block"><i class="fas fa-user-tie"></i> Ubah Profil</button>
        </form>
    </div>
</div>


@endsection