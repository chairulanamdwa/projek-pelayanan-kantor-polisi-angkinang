@extends('layouts.print')

@section('content')
<table class="table table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr class="text-center">
            <th>#</th>
            <th>Status</th>
            <th>Nama Pelapor</th>
            <th>NIK</th>
            <th>Jensi Kelamin</th>
            <th>Alamat</th>
            <th>Deskripsi</th>
            <th>Petugas</th>
        </tr>
    </thead>
    <tbody id="data-nonajax">
        @foreach ($STTLP_selesai as $item)
        <tr>
            <td scope="row">{{$loop->iteration}}</td>
            <td><span class="badge badge-success">{{$item->status}}</span></td>
            <td>{{$item->nama}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->jk}}</td>
            <td>{{$item->alamat}}</td>
            <td>
                <ul>
                    <li>Curat :{{json_decode($item->data,true)['curat']}}</li>
                    <li>Barang Bukti :{{json_decode($item->data,true)['barang_bukti']}}</li>
                </ul>
            </td>
            <td>{{$item->name}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection