@extends('layouts.print')

@section('content')
<table class="table table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr class="text-center">
            <th>#</th>
            <th>Status</th>
            <th>Nama Pelapor</th>
            <th>NIK</th>
            <th>Jensi Kelamin</th>
            <th>Alamat</th>
            <th>Petugas</th>
        </tr>
    </thead>
    <tbody id="data-nonajax">
        @foreach ($SKTLK_diselidiki as $item)
        <tr>
            <td scope="row">{{$loop->iteration}}</td>
            <td><span class="badge badge-info">{{$item->status}}</span></td>
            <td>{{$item->nama}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->jk}}</td>
            <td>{{$item->alamat}}</td>
            <td>{{$item->name}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection