@extends('layouts.print')

@section('content')
<table class="table table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr class="text-center">
            <th>#</th>
            <th>Status</th>
            <th>Nama Pelapor</th>
            <th>NIK</th>
            <th>Jensi Kelamin</th>
            <th>Alamat</th>
            <th>Perihal</th>
            <th>Deskripsi</th>
            <th>Petugas</th>
        </tr>
    </thead>
    <tbody id="data-nonajax">
        @foreach ($SIK_selesai as $item)
        <tr>
            <td scope="row">{{$loop->iteration}}</td>
            <td><span class="badge badge-success">{{$item->status}}</span></td>
            <td>{{$item->nama}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->jk}}</td>
            <td>{{$item->alamat}}</td>
            <td>{{json_decode($item->data,true)['perihal']}}</td>
            <td>
                <ul>
                    <li>Organisasi :{{json_decode($item->data,true)['organisasi']}}</li>
                    <li>Penanggung Jawab :{{json_decode($item->data,true)['penanggung_jawab']}}</li>
                    <li>Macam :{{json_decode($item->data,true)['macam']}}</li>
                    <li>Tanggal Pelaksanaan :{{json_decode($item->data,true)['tgl_pelaksanaan']}}</li>
                    <li>Tempat:{{json_decode($item->data,true)['tempat']}}</li>
                    <li>Rangka:{{json_decode($item->data,true)['rangka']}}</li>
                </ul>
            </td>
            <td>{{$item->name}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection