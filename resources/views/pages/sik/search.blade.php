@forelse ($laporan as $item)
<tr>
    <td scope="row">{{$loop->iteration}}</td>
    <td>
        <a href="#" class="hapus-data" data-id="{{$item->id_sik}}"><i class="fas fa-times fa-fw text-danger"></i></a>
    </td>
    <td>{{$item->nama}}</td>
    <td>{{$item->tmpt_lhr.','.date('d-m-Y',strtotime($item->tgl_lhr))}}</td>
    <td>{{$item->agama}}</td>
    <td>{{$item->alamat}}</td>
    <td>{{$item->pekerjaan}}</td>
    <td>{{$item->organisasi}}</td>
    <td>{{$item->penanggung_jawab}}</td>
    <td>{{$item->perihal}}</td>
</tr>
@empty
<tr class="text-center">
    <td colspan="10">Data Masih Kosong</td>
</tr>
@endforelse