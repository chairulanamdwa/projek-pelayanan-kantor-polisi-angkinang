@extends('layouts.app')


@section('title','Menu Layanan Surat')

@section('content')


<a href="{{route('dashboard')}}" class="badge"><i class="fas fa-tachometer-alt fa-fw"></i> Dashboard</a>

@if (Auth::user()->role != 'user')
<a href="{{route('laporanLayanan')}}" class="badge"><i class="fas fa-file-archive fa-fw"></i> Laporan Layanan</a>
@endif

<br>
<br>
<div class="row">
    @if (Auth::user()->role != 'user')
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('lp-model-a')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> LP Model A <span class="badge badge-primary">new</span>
        </a>
    </div>
    @endif
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('lp-model-b')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> LP Model B
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('sktlk')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> SKTLK
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('sik')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> SIK
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('sttlp')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> STTLP
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('skck')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> SKCK <span class="badge badge-primary">new upload</span>
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('tik')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> Kartu TIK <span class="badge badge-primary">new</span>
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('SPerdamaian')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> Surat Perdamaian <span class="badge badge-primary">new</span>
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('PMasyarakat')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> Pengaduan Masyarakat <span class="badge badge-primary">new</span>
        </a>
    </div>
</div>


{{-- 
<div class="row">
<div class="col-lg-3 mt-4">
    <div class="card shadow">
        <div class="card-header text-center bg-dark text-white">
            <h6>SKTLK ATM</h6>
        </div>
        <div class="card-body text-center">
            <img src="{{url('public/assets/img/icon/fingerprint-biometric-forensic-science-threat-hacker-proof-thumbprint-51645.png')}}"
class="img-fluid" alt="">
</div>
<div class="card-footer text-center bg-secondary">
    <a href="{{route('sktlk-atm')}}" class="btn btn-dark btn-sm">
        Buat Laporan
    </a>
</div>
</div>
</div>
<div class="col-lg-3 mt-4">
    <div class="card shadow">
        <div class="card-header text-center bg-dark text-white">
            <h6> SIK </h6>
        </div>
        <div class="card-body text-center">
            <img src="{{url('public/assets/img/icon/lock-secure-device-connection-data-leak-hack-network-cyber-51656.png')}}"
                class="img-fluid" alt="">
        </div>
        <div class="card-footer text-center bg-secondary">
            <a href="{{route('sik')}}" class="btn btn-dark btn-sm">
                Buat Laporan
            </a>
        </div>
    </div>
</div>
<div class="col-lg-3 mt-4">
    <div class="card shadow">
        <div class="card-header text-center bg-dark text-white">
            <h6> STTLP </h6>
        </div>
        <div class="card-body text-center">
            <img src="{{url('public/assets/img/icon/unzip-data-file-folder-security-device-threat-safety-51654.png')}}"
                class="img-fluid" alt="">
        </div>
        <div class="card-footer text-center bg-secondary">
            <a href="{{route('sttlp')}}" class="btn btn-dark btn-sm">
                Buat Laporan
            </a>
        </div>
    </div>
</div>
<div class="col-lg-3 mt-4">
    <div class="card shadow">
        <div class="card-header text-center bg-dark text-white">
            <h6> SKCK </h6>
        </div>
        <div class="card-body text-center">
            <img src="{{url('public/assets/img/icon/jail-handcuffs-secure-step-attack-trap-hacker-51650.png')}}"
                class="img-fluid" alt="">
        </div>
        <div class="card-footer text-center bg-secondary">
            <a href="{{route('skck')}}" class="btn btn-dark btn-sm">
                Buat Laporan
            </a>
        </div>
    </div>
</div>
</div> --}}


@endsection