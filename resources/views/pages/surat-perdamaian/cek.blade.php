@extends('layouts.app')

@section('title','Cetak Surat Perdamaian')
@section('content')


{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('SPerdamaianShow')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali ke Tabel</a>
<br>
<br>
<form action="{{route('SPerdamaianCetak',$id)}}" method="POST">
    @method('patch')
    @csrf
    <div class="card shadow border-0">
        <div class="card-body">
            <div class="form-group">
                <label for="no_surat"> Nomor Surat <span class="text-danger">*</span></label>
                <input type="text" name="no_surat" id="no_surat" class="form-control form-control-sm" autofocus>
                @if ($errors->has('no_surat'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('no_surat') }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
    <button class="btn btn-warning btn-block mt-2">Cetak Surat</button>
</form>
@endsection