@extends('layouts.app')

@section('title','Surat Perdamaian')

@section('content')


{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('pelayanan-umum')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali ke menu</a>
@if (Auth::user()->role != 'user')
<a href="{{route('SPerdamaianShow')}}" class="badge"><i class="fas fa-file-archive"></i> Arsip Data</a>
@endif
<br>
<br>

<div class="row">
    <div class="col-12">
        <form action="{{route('create-SPerdamaian')}}" method="POST">
            @csrf

            <div class="accordion" id="accordionExample">
                <div class="card shadow border-0">
                    <div class="card-header bg-danger ">
                        <h6 class="mb-0 text-white">
                            Input data Surat Perdamaian
                        </h6>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <h4>Pihak Pertama</h4>
                                    <div class="form-group">
                                        <label for="nik">NIK</label>
                                        <input type="text" name="nik" id="nik" class="form-control form-control-sm">
                                        @if ($errors->has('nik'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('nik') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" name="nama" id="nama" class="form-control form-control-sm">
                                        <input type="hidden" name="id_pelapor" id="id_pelapor"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('id_pelapor'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('id_pelapor') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="tmpt_lhr">Tempat Lahir</label>
                                                <input type="text" name="tmpt_lhr" id="tmpt_lhr"
                                                    class="form-control form-control-sm">
                                                @if ($errors->has('tmpt_lhr'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('tmpt_lhr') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="tgl_lhr">Tanggal Lahir</label>
                                                <input type="date" name="tgl_lhr" id="tgl_lhr"
                                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                                                @if ($errors->has('tgl_lhr'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('tgl_lhr') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="jk">Jenis Kelamin Terlapor</label><br>
                                        <label for="laki_terlapor"><input type="radio" name="jk" id="jk_laki"
                                                value="Laki-Laki">Laki-Laki</label>
                                        <label for="perempun_terlapor"><input type="radio" name="jk" id="jk_perempun"
                                                value="Perempuan">Perempuan</label>
                                        @if ($errors->has('jk'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('jk') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="agama">Agama</label>
                                        <input type="text" name="agama" id="agama" class="form-control form-control-sm">
                                        @if ($errors->has('agama'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('agama') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="pekerjaan">Pekerjaan</label>
                                        <input type="text" name="pekerjaan" id="pekerjaan"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('pekerjaan'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('pekerjaan') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="telpon">No Telp/FAX/Email</label>
                                        <input type="text" name="telpon" id="telpon"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('telpon'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('telpon') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <h4>Pihak Kedua</h4>
                                    <div class="form-group">
                                        <label for="nik2">NIK</label>
                                        <input type="text" name="nik2" id="nik2" class="form-control form-control-sm">
                                        @if ($errors->has('nik2'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('nik2') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="nama2">Nama Lengkap</label>
                                        <input type="text" name="nama2" id="nama2" class="form-control form-control-sm">
                                        <input type="hidden" name="id_pelapor2" id="id_pelapor2"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('id_pelapor2'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('id_pelapor2') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="tmpt_lhr2">Tempat Lahir</label>
                                                <input type="text" name="tmpt_lhr2" id="tmpt_lhr2"
                                                    class="form-control form-control-sm">
                                                @if ($errors->has('tmpt_lhr2'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('tmpt_lhr2') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="tgl_lhr2">Tanggal Lahir</label>
                                                <input type="date" name="tgl_lhr2" id="tgl_lhr2"
                                                    class="form-control form-control-sm" value="{{date('Y-m-d')}}">
                                                @if ($errors->has('tgl_lhr2'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('tgl_lhr2') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="jk2">Jenis Kelamin Terlapor</label><br>
                                        <label for="laki_terlapor"><input type="radio" name="jk2" id="jk_laki2"
                                                value="Laki-Laki">Laki-Laki</label>
                                        <label for="perempun_terlapor"><input type="radio" name="jk2" id="jk_perempun2"
                                                value="Perempuan">Perempuan</label>
                                        @if ($errors->has('jk2'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('jk2') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="agama2">Agama</label>
                                        <input type="text" name="agama2" id="agama2"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('agama2'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('agama2') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="pekerjaan2">Pekerjaan</label>
                                        <input type="text" name="pekerjaan2" id="pekerjaan2"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('pekerjaan2'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('pekerjaan2') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="telpon2">No Telp/FAX/Email</label>
                                        <input type="text" name="telpon2" id="telpon2"
                                            class="form-control form-control-sm">
                                        @if ($errors->has('telpon2'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('telpon2') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_kejadian">Tanggal Kejadian</label>
                                <input type="date" value="{{date('Y-m-d')}}" name="tgl_kejadian" id="tgl_kejadian"
                                    class="form-control form-control-sm">
                                @if ($errors->has('tgl_kejadian'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tgl_kejadian') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="tentang">Kesalah Pahaman Tentang</label>
                                <textarea name="tentang" id="tentang" class="form-control form-control-sm"></textarea>
                                @if ($errors->has('tentang'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tentang') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-warning btn-block mt-2">Buat Data</button>
        </form>
    </div>
</div>


@endsection


@section('js')
<script>
    $('#nik').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_pelapor').val('');
        $('#nama').val('');
        $('#tmpt_lhr').val('');
        $('#agama').val('')
        $('#pekerjaan').val('')
        $('#alamat').val('')
        $('#tgl_lhr').val('');
        $('#telpon').val('')
        $('#jk_laki').attr('checked', false);
        $('#jk_perempun').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik').addClass('is-invalid')
            }else{
                $('#nik').removeClass('is-invalid')
                $('#nik').addClass('is-valid')
                $('#id_pelapor').val(json.id_masyarakat)
                $('#nama').val(json.nama)
                $('#agama').val(json.agama)
                $('#tmpt_lhr').val(json.tmpt_lhr)
                $('#tgl_lhr').val(json.tgl_lhr)
                $('#pekerjaan').val(json.pekerjaan)
                $('#alamat').val(json.alamat)
                $('#telpon').val(json.telpon)
                if (json.jk == 'L') {
                    $('#jk_laki').attr('checked', 'checked');
                }else{
                    $('#jk_perempun').attr('checked', 'checked');
                }
            }
        }
        })
        });    
    $('#nik2').on('input',function(){
        const data = $(this).val();
        const token = $('meta[name="csrf-token"]').attr('content');
        $('#id_pelapor2').val('');
        $('#nama2').val('');
        $('#tmpt_lhr2').val('');
        $('#agama2').val('')
        $('#pekerjaan2').val('')
        $('#alamat2').val('')
        $('#tgl_lhr2').val('');
        $('#telpon2').val('')
        $('#jk_laki2').attr('checked', false);
        $('#jk_perempun2').attr('checked', false);
        $.ajax({
        'url':"{{route('autoCompliteMasyarakat')}}",
        'type':'post',
        'data':{data:data,'_token':token},
        success:(data)=>{
            const json = JSON.parse(data);
            if (json == null) {
                $('#nik2').addClass('is-invalid')
            }else{
                $('#nik2').removeClass('is-invalid')
                $('#nik2').addClass('is-valid')
                $('#id_pelapor2').val(json.id_masyarakat)
                $('#nama2').val(json.nama)
                $('#agama2').val(json.agama)
                $('#tmpt_lhr2').val(json.tmpt_lhr)
                $('#tgl_lhr2').val(json.tgl_lhr)
                $('#pekerjaan2').val(json.pekerjaan)
                $('#alamat2').val(json.alamat)
                $('#telpon2').val(json.telpon)
                if (json.jk == 'L') {
                    $('#jk_laki2').attr('checked', 'checked');
                }else{
                    $('#jk_perempun2').attr('checked', 'checked');
                }
            }
        }
        })
        });    
</script>
@endsection