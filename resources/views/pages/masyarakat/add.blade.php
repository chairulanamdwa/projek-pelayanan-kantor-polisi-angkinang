@extends('layouts.app')
@section('title','Data Masyarakat')

@section('content')


<a href="{{route('masyarakat')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali</a>
<br>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <form action="{{route('masyarakatStore')}}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="nama">Nama <span class="text-danger">*</span></label>
                        <input type="text" name="nama" id="nama" class="form-control" autofocus>
                        @if ($errors->has('nama'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nik">NIK <span class="text-danger">*</span></label>
                        <input type="number" name="nik" id="nik" class="form-control">
                        @if ($errors->has('nik'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nik') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="kk">KK <span class="text-danger">*</span></label>
                        <input type="number" name="kk" id="kk" class="form-control">
                        @if ($errors->has('kk'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('kk') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="tmpt_lhr">Tempat Lahir <span class="text-danger">*</span></label>
                                <input type="text" name="tmpt_lhr" id="tmpt_lhr" class="form-control">
                                @if ($errors->has('tmpt_lhr'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tmpt_lhr') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="tgl_lhr">Tanggal Lahir <span class="text-danger">*</span></label>
                                <input type="date" name="tgl_lhr" id="tgl_lhr" class="form-control">
                                @if ($errors->has('tgl_lhr'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tgl_lhr') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jk">Jenis Kelamin <span class="text-danger">*</span></label>
                        <label for="jk_l"><input type="radio" name="jk" id="jk_l" value="L"> Laki-Laki</label>
                        <label for="jk_p"><input type="radio" name="jk" id="jk_p" value="P"> Perempuan</label>
                        @if ($errors->has('kk'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('kk') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="agama">Agama <span class="text-danger">*</span></label>
                        <select name="agama" id="agama" class="form-control">
                            <option value="">..Pilih..</option>
                            <option value="Islam">ISLAM</option>
                            <option value="Kristen Protestan">KRISTEN PROTESTAN</option>
                            <option value="Katolik">KATOLIK</option>
                            <option value="Hindu">HINDU</option>
                            <option value="Buddha">BUDDHA</option>
                            <option value="Kong Hu Cu">KONG HU CU</option>
                        </select>
                        @if ($errors->has('agama'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('agama') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan">Pekerjaan <span class="text-danger">*</span></label>
                        <input type="text" name="pekerjaan" id="pekerjaan" class="form-control">
                        @if ($errors->has('pekerjaan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telpon">HP/Telpon </label>
                        <input type="number" name="telpon" id="telpon" class="form-control">
                        @if ($errors->has('telpon'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('telpon') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="gol_darah">Golongan Darah</label>
                        <select name="gol_darah" id="gol_darah" class="form-control">
                            <option value="">..Pilih..</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="AB">AB</option>
                            <option value="O">O</option>
                        </select>
                        @if ($errors->has('gol_darah'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('gol_darah') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pddk_terakhir">Pendidikan Terakhir <span class="text-danger">*</span></label>
                        <input type="text" name="pddk_terakhir" id="pddk_terakhir" class="form-control">
                        @if ($errors->has('pddk_terakhir'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pddk_terakhir') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="stts_kawin">Status Kawin <span class="text-danger">*</span></label>
                        <select name="stts_kawin" id="stts_kawin" class="form-control">
                            <option value="">..Pilih..</option>
                            <option value="Kawin">Kawin</option>
                            <option value="Belum Kawin">Belum Kawin</option>
                            <option value="Duda">Duda</option>
                            <option value="Janda">Janda</option>
                        </select>
                        @if ($errors->has('stts_kawin'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('stts_kawin') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat <span class="text-danger">*</span></label>
                        <textarea name="alamat" id="alamat" class="form-control"></textarea>
                        @if ($errors->has('alamat'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="rt">RT <span class="text-danger">*</span></label>
                                <input type="number" name="rt" id="rt" class="form-control">
                                @if ($errors->has('rt'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('rt') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="rw">RW <span class="text-danger">*</span></label>
                                <input type="number" name="rw" id="rw" class="form-control">
                                @if ($errors->has('rw'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('rw') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-block">Tambah Data Masyarakat</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection