@extends('layouts.app')
@section('title','Edit Data Masyarakat')

@section('content')


<a href="{{route('masyarakat')}}" class="badge"><i class="fas fa-arrow-left"></i> Kembali</a>
<br>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <form action="{{route('masyarakatUpdate',$masyarakat->id_masyarakat)}}" method="POST">
                    @method('PATCH')
                    @csrf

                    <div class="form-group">
                        <label for="nama">Nama <span class="text-danger">*</span></label>
                        <input type="text" name="nama" id="nama" class="form-control" value="{{$masyarakat->nama}}"
                            autofocus>
                        @if ($errors->has('nama'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nama') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="nik">NIK <span class="text-danger">*</span></label>
                        <input type="number" name="nik" id="nik" class="form-control" value="{{$masyarakat->nik}}"
                            readonly>
                        @if ($errors->has('nik'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('nik') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="kk">KK <span class="text-danger">*</span></label>
                        <input type="number" name="kk" id="kk" class="form-control" value="{{$masyarakat->kk}}">
                        @if ($errors->has('kk'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('kk') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="tmpt_lhr">Tempat Lahir <span class="text-danger">*</span></label>
                                <input type="text" name="tmpt_lhr" id="tmpt_lhr" class="form-control"
                                    value="{{$masyarakat->tmpt_lhr}}">
                                @if ($errors->has('tmpt_lhr'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tmpt_lhr') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="tgl_lhr">Tanggal Lahir <span class="text-danger">*</span></label>
                                <input type="date" name="tgl_lhr" id="tgl_lhr" class="form-control"
                                    value="{{date('Y-m-d',strtotime($masyarakat->tgl_lhr))}}">
                                @if ($errors->has('tgl_lhr'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('tgl_lhr') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jk">Jenis Kelamin <span class="text-danger">*</span></label>
                        <label for="jk_l"><input type="radio" name="jk" id="jk_l" value="L" @if ($masyarakat->jk == 'L')
                            checked @endif > Laki-Laki</label>
                        <label for="jk_p"><input type="radio" name="jk" id="jk_p" value="P" @if ($masyarakat->jk == 'P')
                            checked @endif> Perempuan</label>
                        @if ($errors->has('kk'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('kk') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="agama">Agama <span class="text-danger">*</span></label>
                        <select name="agama" id="agama" class="form-control">
                            <option value="">..Pilih..</option>
                            <option @if ($masyarakat->agama == 'Islam') selected @endif value="Islam">ISLAM</option>
                            <option @if ($masyarakat->agama == 'Kristen Protestan') selected @endif value="Kristen
                                Protestan">KRISTEN
                                PROTESTAN</option>
                            <option @if ($masyarakat->agama == 'Katolik') selected @endif value="Katolik">KATOLIK
                            </option>
                            <option @if ($masyarakat->agama == 'Hindu') selected @endif value="Hindu">HINDU</option>
                            <option @if ($masyarakat->agama == 'Buddha') selected @endif value="Buddha">BUDDHA</option>
                            <option @if ($masyarakat->agama == 'Kong Hu Cu') selected @endif value="Kong Hu Cu">KONG HU
                                CU
                            </option>
                        </select>
                        @if ($errors->has('agama'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('agama') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan">Pekerjaan <span class="text-danger">*</span></label>
                        <input type="text" name="pekerjaan" id="pekerjaan" class="form-control"
                            value="{{$masyarakat->pekerjaan}}">
                        @if ($errors->has('pekerjaan'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pekerjaan') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="telpon">HP/Telpon </label>
                        <input type="number" name="telpon" id="telpon" class="form-control"
                            value="{{$masyarakat->telpon}}">
                        @if ($errors->has('telpon'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('telpon') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="gol_darah">Golongan Darah</label>
                        <select name="gol_darah" id="gol_darah" class="form-control">
                            <option value="">..Pilih..</option>
                            <option @if ($masyarakat->gol_darah == 'A') selected @endif value="A">A</option>
                            <option @if ($masyarakat->gol_darah == 'B') selected @endif value="B">B</option>
                            <option @if ($masyarakat->gol_darah == 'AB') selected @endif value="AB">AB</option>
                            <option @if ($masyarakat->gol_darah == 'O') selected @endif value="O">O</option>
                        </select>
                        @if ($errors->has('gol_darah'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('gol_darah') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pddk_terakhir">Pendidikan Terakhir <span class="text-danger">*</span></label>
                        <input type="text" name="pddk_terakhir" id="pddk_terakhir" class="form-control"
                            value="{{$masyarakat->pddk_terakhir}}">
                        @if ($errors->has('pddk_terakhir'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('pddk_terakhir') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="stts_kawin">Status Kawin <span class="text-danger">*</span></label>
                        <select name="stts_kawin" id="stts_kawin" class="form-control">
                            <option value="">..Pilih..</option>
                            <option @if ($masyarakat->stts_kawin == 'Kawin') selected @endif value="Kawin">Kawin
                            </option>
                            <option @if ($masyarakat->stts_kawin == 'Belum Kawin') selected @endif value="Belum
                                Kawin">Belum Kawin
                            </option>
                            <option @if ($masyarakat->stts_kawin == 'Duda') selected @endif value="Duda">Duda</option>
                            <option @if ($masyarakat->stts_kawin == 'Janda') selected @endif value="Janda">Janda
                            </option>
                        </select>
                        @if ($errors->has('stts_kawin'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('stts_kawin') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat <span class="text-danger">*</span></label>
                        <textarea name="alamat" id="alamat" class="form-control">{{$masyarakat->alamat}}</textarea>
                        @if ($errors->has('alamat'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="rt">RT <span class="text-danger">*</span></label>
                                <input type="number" name="rt" id="rt" class="form-control" value="{{$masyarakat->rt}}">
                                @if ($errors->has('rt'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('rt') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="rw">RW <span class="text-danger">*</span></label>
                                <input type="number" name="rw" id="rw" class="form-control" value="{{$masyarakat->rw}}">
                                @if ($errors->has('rw'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('rw') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning btn-block">Ganti Data Masyarakat</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection