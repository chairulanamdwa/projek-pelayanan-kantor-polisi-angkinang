@extends('layouts.app')
@section('title','Data Masyarakat')

@section('content')

{{-- Alert --}}
@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif
{{-- Akhir Alert --}}

<a href="{{route('masyarakatAdd')}}" class="badge"><i class="fas fa-plus"></i>Tambah Masyarakat</a>
<br>
<br>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-hover table-bordered dataTable table-responsive" width="100%" cellspacing="0">
            <thead class="bg-warning">
                <tr class="text-center text-white">
                    <th>#</th>
                    <th>Aksi <span class="text-danger">*</span></th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>TTL</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Pekerjaan</th>
                    <th>Telpon</th>
                    <th>Golongan Darah</th>
                    <th>Pendidikan Terahir</th>
                    <th>Setatus Kawin</th>
                    <th>Alamat</th>
                    <th>RT</th>
                    <th>RW</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($masyarakat as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>
                        <form action="{{route('masyarakatDestroy',$item->id_masyarakat)}}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger btn-sm"
                                onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')">
                                Hapus
                            </button>
                        </form>
                        <a href="{{route('masyarakatEdit',$item->id_masyarakat)}}"
                            class="btn btn-secondary btn-sm">Edit</a>
                        <a href="{{route('masyarakatKeluarga',$item->kk)}}" class="btn btn-info btn-sm">Keluarga</a>
                    </td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->nik}}</td>
                    <td>{{$item->tmpt_lhr.','.date('d/m/Y',strtotime($item->tgl_lhr))}}</td>
                    <td>{{$item->jk}}</td>
                    <td>{{$item->agama}}</td>
                    <td>{{$item->pekerjaan}}</td>
                    <td>{{$item->telpon}}</td>
                    <td>{{$item->gol_darah}}</td>
                    <td>{{$item->pddk_terakhir}}</td>
                    <td>{{$item->stts_kawin}}</td>
                    <td>{{$item->alamat}}</td>
                    <td>{{$item->rt}}</td>
                    <td>{{$item->rw}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection