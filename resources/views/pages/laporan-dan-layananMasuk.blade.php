@extends('layouts.app')


@section('title','Menu Layanan Masuk dan Laporan')

@section('content')


<a href="{{route('dashboard')}}" class="badge"><i class="fas fa-tachometer-alt fa-fw"></i> Dashboard</a>
<a href="{{route('pelayanan-umum')}}" class="badge"><i class="fas fa-hand-holding-heart fa-fw"></i> Menu Layanan</a>
<br>
<br>
<div class="row">
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('lpModelAShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> LP Model A <span class="badge badge-primary">new</span>
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('lpModelBShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> LP Model B
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('sktlkShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> SKTLK
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('sikShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> SIK
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('sttlpShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> STTLP
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('skckShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> SKCK <span class="badge badge-primary">new upload</span>
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="#" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> Kartu TIK <span class="badge badge-primary">new</span>
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('SPerdamaianShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> Surat Perdamaian <span class="badge badge-primary">new</span>
        </a>
    </div>
    <div class="mt-3 col-6 col-md-4">
        <a href="{{route('PMasyarakatShow')}}" class="btn btn-light shadow btn-block text-left">
            <i class="fas fa-file-alt fa-fw"></i> Pengaduan Masyarakat <span class="badge badge-primary">new</span>
        </a>
    </div>
</div>

@endsection