@forelse ($petugas as $item)
<tr>
    <td scop="row">{{$loop->iteration}}</td>
    <td>
        <a href="{{route('petugas-edit',$item->id)}}" class="text-success"><i class="fas fa-edit fa-fw"></i></a>
        <a href="" class="text-danger hapus-data" data-id="{{$item->id}}"><i class="fas fa-trash fa-fw"></i></a>
    </td>
    <td>{{$item->name}}</td>
    <td>{{$item->nik}}</td>
    <td>{{$item->nip}}</td>
    <td>{{$item->pangkat}}</td>
    <td>{{$item->jk}}</td>
    <td>{{$item->tmpt_lhr.','.$item->tgl_lhr}}</td>
    <td>{{$item->alamat}}</td>
</tr>
@empty
<tr class="text-center">
    <td colspan="9">Data Masih Kosong</td>
</tr>
@endforelse