@extends('layouts.app')

@section('title','Petugas')
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<div class="row">
    <div class="col-6">
        <a href="{{route('petugas')}}" class="badge"><i class="fas fa-arrow-left fa-fw"></i>Kembali</a>
    </div>
</div>

<div class="row my-4">
    <div class="col-lg-4">
        <form action="{{route('petugas-store')}}" method="POST">
            @csrf
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control form-control-sm"
                    placeholder="name Petugas">
                @if ($errors->has('name'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control form-control-sm" placeholder="email">
                @if ($errors->has('email'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="number" name="nik" id="nik" class="form-control form-control-sm" placeholder="NIK">
                @if ($errors->has('nik'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('nik') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="number" name="nip" id="nip" class="form-control form-control-sm" placeholder="NIP">
                @if ($errors->has('nip'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('nip') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="laki"><input type="radio" name="jk" id="laki" value="L">Laki-Laki</label>
                <label for="perempuan"><input type="radio" name="jk" id="perempuan" value="P">Perempuan</label>
                @if ($errors->has('jk'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('jk') }}</strong>
                </span>
                @endif
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <input type="text" name="tmpt_lhr" id="tmpt_lhr" class="form-control form-control-sm"
                            placeholder="Tempat Lahir">
                        @if ($errors->has('tmpt_lhr'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tmpt_lhr') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input type="date" name="tgl_lhr" id="tgl_lhr" class="form-control form-control-sm"
                            placeholder="Tanggal Lahir">
                        @if ($errors->has('tgl_lhr'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tgl_lhr') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="admin"><input type="radio" name="role" id="admin" value="admin">Admin</label>
                <label for="petugas"><input type="radio" name="role" id="petugas" value="petugas">Petugas</label>
                <label for="user"><input type="radio" name="role" id="user" value="petugas">User</label>
                @if ($errors->has('jk'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('jk') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="text" name="pangkat" id="pangkat" class="form-control form-control-sm"
                    placeholder="Pangkat">
                @if ($errors->has('pangkat'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('pangkat') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <textarea name="alamat" id="alamat" class="form-control form-control-sm" placeholder="Alamat"
                    rows="4"></textarea>
                @if ($errors->has('alamat'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('alamat') }}</strong>
                </span>
                @endif
            </div>
            <button class="btn btn-dark btn-sm btn-block"><i class="fas fa-user-tie"></i> Tambah Petugas</button>
        </form>
    </div>
</div>


@endsection