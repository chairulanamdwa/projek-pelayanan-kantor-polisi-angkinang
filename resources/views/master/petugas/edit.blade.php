@extends('layouts.app')

@section('title','Petugas')
@section('content')

@if (session('message'))
<div class="alert alert-success mt-4">
    {{session('message')}}
</div>
@endif

<div class="row">
    <div class="col-6">
        <a href="{{route('petugas')}}" class="btn btn-secondary btn-sm"><i class="fas fa-arrow-left fa-fw"></i> Tambah
            Kembali</a>
    </div>
</div>

<div class="row my-4">
    <div class="col-lg-4">
        <form action="{{route('petugas-update',$petugas->id)}}" method="POST">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control form-control-sm" placeholder="name Petugas"
                    value="{{$petugas->name}}">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control form-control-sm" placeholder="email"
                    value="{{$petugas->email}}" readonly>
                @if ($errors->has('email'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="number" name="nik" id="nik" class="form-control form-control-sm" placeholder="NIK"
                    value="{{$petugas->nik}}">
                @error('nik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="number" name="nip" id="nip" class="form-control form-control-sm" placeholder="NIP"
                    value="{{$petugas->nip}}">
                @error('nip')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="laki"><input type="radio" name="jk" id="laki" value="L" @if ($petugas->jk == 'L') checked
                    @endif>Laki-Laki</label>
                <label for="perempuan"><input type="radio" name="jk" id="perempuan" value="P" @if ($petugas->jk == 'P')
                    checked
                    @endif>Perempuan</label>
                @error('jk')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <input type="text" name="tmpt_lhr" id="tmpt_lhr" class="form-control form-control-sm"
                            placeholder="Tempat Lahir" value="{{$petugas->tmpt_lhr}}">
                        @error('tmpt_lhr')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input type="date" name="tgl_lhr" id="tgl_lhr" class="form-control form-control-sm"
                            placeholder="Tanggal Lahir" value="{{date('Y-m-d',strtotime($petugas->tgl_lhr))}}">
                        @error('tgl_lhr')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="admin"><input type="radio" name="role" id="admin" @if ($petugas->role == 'admin') checked
                    @endif value="admin">Admin</label>
                <label for="petugas"><input type="radio" name="role" id="petugas" @if ($petugas->role == 'petugas')
                    checked
                    @endif value="petugas">Petugas</label>
                <label for="user"><input type="radio" name="role" id="user" @if ($petugas->role == 'user') checked
                    @endif value="petugas">User</label>
                @if ($errors->has('jk'))
                <span class="text-danger" role="alert">
                    <strong>{{ $errors->first('jk') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="text" name="pangkat" id="pangkat" class="form-control form-control-sm"
                    placeholder="Pangkat" value="{{$petugas->pangkat}}">
                @error('pangkat')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <textarea name="alamat" id="alamat" class="form-control form-control-sm" placeholder="Alamat"
                    rows="4">{{$petugas->alamat}}</textarea>
                @error('alamat')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <button class="btn btn-dark btn-sm btn-block"><i class="fas fa-user-tie"></i> Ubah Data
                {{$petugas->nama}}</button>
        </form>
    </div>
</div>


@endsection