@extends('layouts.app')

@section('title','Petugas')
@section('content')

<div id="alert"></div>

<div class="row justify-content-end">
    <div class="col-6">
        <a href="{{route('tambah-petugas')}}" class="btn btn-secondary btn-sm"><i class="fas fa-plus fa-fw"></i> Tambah
            Petugas Polisi</a>
    </div>
    <div class="col-6">
        <div class="input-group mb-3">
            <input type="text" class="form-control form-control-sm" name="cari" id="cari" autocomplete="off"
                placeholder="Cari Biodata..." autofocus>
            <div class="input-group-append">
                <span class="input-group-text bg-secondary text-white"><i class="fas fa-search fa-fw"></i></span>
            </div>
        </div>

    </div>
</div>

<div class="row my-4">
    <div class="col-lg-12">
        <table class="table table-hover table-bordered">
            <thead class="thead-dark">
                <tr class="text-center">
                    <th>#</th>
                    <th>Aksi <span class="text-danger">*</span></th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>NIP</th>
                    <th>Pangkat</th>
                    <th>Jenis Kelamin</th>
                    <th>TTL</th>
                    <th>Email</th>
                    <th>Alamat</th>
                </tr>
            </thead>
            <tbody id="data-nonajax">
                @forelse ($petugas as $item)
                <tr>
                    <td scop="row">{{$loop->iteration}}</td>
                    <td>
                        <a href="{{route('petugas-edit',$item->id)}}" class="text-success"><i
                                class="fas fa-edit fa-fw"></i></a>
                        <a href="" class="text-danger hapus-data" data-id="{{$item->id}}"><i
                                class="fas fa-trash fa-fw"></i></a>
                    </td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->nik}}</td>
                    <td>{{$item->nip}}</td>
                    <td>{{$item->pangkat}}</td>
                    <td>{{$item->jk}}</td>
                    <td>{{$item->tmpt_lhr.','.$item->tgl_lhr}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->alamat}}</td>
                </tr>
                @empty
                <tr class="text-center">
                    <td colspan="10">Data Masih Kosong</td>
                </tr>
                @endforelse
            </tbody>
            <tbody id="data-ajax"></tbody>
        </table>
        <div id="links">
            {{$petugas->links()}}
        </div>
    </div>
</div>


@endsection

@section('js')
<script>
    // =============function javascript===========
    function hapus(){
            $('.hapus-data').on('click',function(e){
                const konfirmasi = confirm('Apakah anda yakin ingin menghapus?');
                e.preventDefault();
                const data = $(this).data('id');
                const token = $('meta[name="csrf-token"]').attr("content");

                if (konfirmasi) {
                    $.ajax({
                    url:"{{route('petugas-delete')}}",
                    type:"DELETE",
                    data:{data:data,"_token":token},
                    success:function(data){
                        $('#alert').html(`<div class="alert alert-success mt-4"> 
                                        <div class="clearfix">
                                            `+data+`
                                            <div class="spinner-border float-right" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                            </div>
                                        </div>`);
                        setTimeout(function(){location.reload()},2000);
                    }
                });
                }
            });
        }

    $('#cari').on('input',function(){
            const val = $('#cari').val();
            const token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url:"{{route('petugas-cari')}}",
                type:"POST",
                data:{data:val,"_token":token},
                success:function(data)
                {
                    $('#data-nonajax').hide();
                    $('#data-ajax').html(data);
                    if (val == '') {
                    $('#links').show();
                    }else{
                    $('#links').hide();
                    }
                    hapus();
                }
            });
        });

        hapus()
</script>
@endsection