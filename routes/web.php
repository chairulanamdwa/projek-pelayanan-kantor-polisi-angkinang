<?php

Route::group(['middleware' => ['auth', 'petugas']], function () {

    Route::get('/laporan-dan-layananMasuk', 'PagesController@layananMasuk')->name('laporanLayanan');

    // MASYARAKAT   
    Route::get('/masyarakat', 'MasyarakatController@index')->name('masyarakat');
    Route::get('/masyarakat-add', 'MasyarakatController@create')->name('masyarakatAdd');
    Route::post('/masyarakat', 'MasyarakatController@store')->name('masyarakatStore');
    Route::get('/masyarakat/{masyarakat}', 'MasyarakatController@edit')->name('masyarakatEdit');
    Route::patch('/masyarakat/{masyarakat}', 'MasyarakatController@update')->name('masyarakatUpdate');
    Route::delete('/masyarakat/{masyarakat}/delete', 'MasyarakatController@destroy')->name('masyarakatDestroy');
    Route::get('/masyarakat/{masyarakat}/keluarga', 'MasyarakatController@keluarga')->name('masyarakatKeluarga');

    // LPMODEL A
    Route::get('/lp-model-a/print/selidiki', 'LPmodelAController@printSelidiki')->name('lpModelAPrintSelidiki');
    Route::get('/lp-model-a/print/selesai', 'LPmodelAController@printSelesai')->name('lpModelAPrintSelesai');
    Route::get('/lp-model-a/show', 'LPmodelAController@show')->name('lpModelAShow');
    Route::delete('/lp-model-a/{lp_model_a}/delete', 'LPmodelAController@destroy')->name('lpModelADestroy');
    Route::get('/lp-model-a/{lp_model_a}/cek', 'LPmodelAController@edit')->name('lpModelACek');
    Route::patch('/lp-model-a/{lp_model_a}/cetak', 'LPmodelAController@cetak')->name('lpModelACetak');
    Route::patch('/lp-model-a/{lp_model_a}/selesai', 'LPmodelAController@update')->name('lpModelASelesai');


    // LPMODEL B
    Route::get('/lp-model-b/print/selidiki', 'LPmodelBController@printSelidiki')->name('lpModelBPrintSelidiki');
    Route::get('/lp-model-b/print/selesai', 'LPmodelBController@printSelesai')->name('lpModelBPrintSelesai');
    Route::get('/lp-model-b/show', 'LPmodelBController@show')->name('lpModelBShow');
    Route::delete('/lp-model-b/{lp_model_b}/delete', 'LPmodelBController@destroy')->name('lpModelBDestroy');
    Route::get('/lp-model-b/{lp_model_b}/cek', 'LPmodelBController@edit')->name('lpModelBCek');
    Route::patch('/lp-model-b/{lp_model_b}/cetak', 'LPmodelBController@cetak')->name('lpModelBCetak');
    Route::patch('/lp-model-b/{lp_model_b}/selesai', 'LPmodelBController@update')->name('lpModelBSelesai');


    // SKTLK 
    Route::get('/sktlk/print/selidiki', 'SktlkController@printSelidiki')->name('sktlkPrintSelidiki');
    Route::get('/sktlk/print/selesai', 'SktlkController@printSelesai')->name('sktlkPrintSelesai');
    Route::get('/sktlk/show', 'SktlkController@show')->name('sktlkShow');
    Route::delete('/sktlk/{sktlk}/delete', 'SktlkController@destroy')->name('sktlkDestroy');
    Route::get('/sktlk/{sktlk}/cek', 'SktlkController@edit')->name('sktlkCek');
    Route::patch('/sktlk/{sktlk}/cetak', 'SktlkController@cetak')->name('sktlkCetak');
    Route::patch('/sktlk/{sktlk}/selesai', 'SktlkController@update')->name('sktlkSelesai');


    // SIK
    Route::get('/sik/print/selidiki', 'SIKController@printSelidiki')->name('sikPrintSelidiki');
    Route::get('/sik/print/selesai', 'SIKController@printSelesai')->name('sikPrintSelesai');
    Route::get('/sik/show', 'SIKController@show')->name('sikShow');
    Route::delete('/sik/{sik}/delete', 'SIKController@destroy')->name('sikDestroy');
    Route::get('/sik/{sik}/cek', 'SIKController@edit')->name('sikCek');
    Route::patch('/sik/{sik}/cetak', 'SIKController@cetak')->name('sikCetak');
    Route::patch('/sik/{sik}/selesai', 'SIKController@update')->name('sikSelesai');


    // STTLP
    Route::get('/sttlp/print/selidiki', 'STTLPController@printSelidiki')->name('sttlpPrintSelidiki');
    Route::get('/sttlp/print/selesai', 'STTLPController@printSelesai')->name('sttlpPrintSelesai');
    Route::get('/sttlp/show', 'STTLPController@show')->name('sttlpShow');
    Route::delete('/sttlp/{sttlp}/delete', 'STTLPController@destroy')->name('sttlpDestroy');
    Route::get('/sttlp/{sttlp}/cek', 'STTLPController@edit')->name('sttlpCek');
    Route::patch('/sttlp/{sttlp}/cetak', 'STTLPController@cetak')->name('sttlpCetak');
    Route::patch('/sttlp/{sttlp}/selesai', 'STTLPController@update')->name('sttlpSelesai');


    // SKCK
    Route::get('/skck/print/selesai', 'SKCKController@printSelesai')->name('skckPrintSelesai');
    Route::get('/skck/show', 'SKCKController@show')->name('skckShow');
    Route::delete('/skck/{skck}/delete', 'SKCKController@destroy')->name('skckDestroy');
    Route::get('/skck/{skck}/cek', 'SKCKController@edit')->name('skckCek');
    Route::patch('/skck/{skck}/cetak', 'SKCKController@cetak')->name('skckCetak');
    Route::patch('/skck/{skck}/selesai', 'SKCKController@update')->name('skckSelesai');


    // SURAT PERDAMAIAN
    Route::get('/surat-perdamaian/print/selesai', 'SPerdamaianController@printSelesai')->name('SPerdamaianPrintSelesai');
    Route::get('/surat-perdamaian/show', 'SPerdamaianController@show')->name('SPerdamaianShow');
    Route::delete('/surat-perdamaian/{surat_perdamaian}/delete', 'SPerdamaianController@destroy')->name('SPerdamaianDestroy');
    Route::get('/surat-perdamaian/{surat_perdamaian}/cek', 'SPerdamaianController@edit')->name('SPerdamaianCek');
    Route::patch('/surat-perdamaian/{surat_perdamaian}/cetak', 'SPerdamaianController@cetak')->name('SPerdamaianCetak');
    Route::patch('/surat-perdamaian/{surat_perdamaian}/selesai', 'SPerdamaianController@update')->name('SPerdamaianSelesai');


    // PENGADUAN MASYARAKAT
    Route::get('/pengaduan-masyarakat/print/selidiki', 'PengaduanMasyarakatController@printSelidiki')->name('PMasyarakatPrintSelidiki');
    Route::get('/pengaduan-masyarakat/print/selesai', 'PengaduanMasyarakatController@printSelesai')->name('PMasyarakatPrintSelesai');
    Route::get('/pengaduan-masyarakat/show', 'PengaduanMasyarakatController@show')->name('PMasyarakatShow');
    Route::delete('/pengaduan-masyarakat/{pengaduan_masyarakat}/delete', 'PengaduanMasyarakatController@destroy')->name('PMasyarakatDestroy');
    Route::get('/pengaduan-masyarakat/{pengaduan_masyarakat}/cek', 'PengaduanMasyarakatController@edit')->name('PMasyarakatCek');
    Route::patch('/pengaduan-masyarakat/{pengaduan_masyarakat}/cetak', 'PengaduanMasyarakatController@cetak')->name('PMasyarakatCetak');
    Route::patch('/pengaduan-masyarakat/{pengaduan_masyarakat}/selesai', 'PengaduanMasyarakatController@update')->name('PMasyarakatSelesai');


    // TIK
    Route::get('/tik/print/selidiki', 'TIKController@printSelidiki')->name('TIKPrintSelidiki');
    Route::get('/tik/print/selesai', 'TIKController@printSelesai')->name('TIKPrintSelesai');
    Route::get('/tik/show', 'TIKController@show')->name('TIKShow');
    Route::delete('/tik/{tik}/delete', 'TIKController@destroy')->name('TIKDestroy');
    Route::get('/tik/{tik}/cek', 'TIKController@edit')->name('TIKCek');
    Route::patch('/tik/{tik}/cetak', 'TIKController@cetak')->name('TIKCetak');
    Route::patch('/tik/{tik}/selesai', 'TIKController@update')->name('TIKSelesai');



    // PETUGAS
    Route::get('/petugas', 'PetugasController@index')->name('petugas');
    Route::get('/petugas-add', 'PetugasController@create')->name('tambah-petugas');
    Route::post('/petugas-store', 'PetugasController@store')->name('petugas-store');
    Route::post('/petugas-cari', 'PetugasController@cari')->name('petugas-cari');
    Route::get('/petugas-edit/{id}', 'PetugasController@edit')->name('petugas-edit');
    Route::patch('/petugas-update/{id}', 'PetugasController@update')->name('petugas-update');
    Route::delete('/petugas-delete', 'PetugasController@destroy')->name('petugas-delete');
});
Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'PagesController@dashboard')->name('dashboard');
    Route::get('/pelayanan-umum', 'PagesController@pelayananUmum')->name('pelayanan-umum');

    // PROFIL
    Route::get('/profil', 'UserProfilController@index')->name('profil');
    Route::patch('/profil', 'UserProfilController@update')->name('profil');


    Route::post('/autocomplite-masyarakat', 'AutoCompliteController@getMasyarakat')->name('autoCompliteMasyarakat');

    Route::get('/user-profil', 'UserProfilController@index')->name('userProfil');


    // LPMODEL A
    Route::get('/lp-model-a', 'LPmodelAController@index')->name('lp-model-a');
    Route::POST('/lp-model-a/tambah', 'LPmodelAController@store')->name('create-model-a');
    Route::POST('/lp-model-a/cari', 'LPmodelAController@search')->name('search-model-a');

    // LPMODEL B
    Route::get('/lp-model-b', 'LPmodelBController@index')->name('lp-model-b');
    Route::POST('/lp-model-b/tambah', 'LPmodelBController@store')->name('create-model-b');
    Route::POST('/lp-model-b/cari', 'LPmodelBController@search')->name('search-model-b');

    // SKTLK 
    Route::get('/sktlk', 'SktlkController@index')->name('sktlk');
    Route::POST('/sktlk/tambah', 'SktlkController@store')->name('create-sktlk');
    Route::POST('/sktlk/cari', 'SktlkController@search')->name('search-sktlk');

    // SIK
    Route::get('/sik', 'SIKController@index')->name('sik');
    Route::POST('/sik/tambah', 'SIKController@store')->name('create-sik');
    Route::POST('/sik/cari', 'SIKController@search')->name('search-sik');

    // STTLP
    Route::get('/sttlp', 'STTLPController@index')->name('sttlp');
    Route::POST('/sttlp/tambah', 'STTLPController@store')->name('create-sttlp');
    Route::POST('/sttlp/cari', 'STTLPController@search')->name('search-sttlp');

    // SKCK
    Route::get('/skck', 'SKCKController@index')->name('skck');
    Route::POST('/skck/tambah', 'SKCKController@store')->name('create-skck');
    Route::POST('/skck/cari', 'SKCKController@search')->name('search-skck');

    // SURAT PERDAMAIAN
    Route::get('/surat-perdamaian', 'SPerdamaianController@index')->name('SPerdamaian');
    Route::POST('/surat-perdamaian/tambah', 'SPerdamaianController@store')->name('create-SPerdamaian');
    Route::POST('/surat-perdamaian/cari', 'SPerdamaianController@search')->name('search-SPerdamaian');

    // PENGADUAN MASYARAKAT
    Route::get('/pengaduan-masyarakat', 'PengaduanMasyarakatController@index')->name('PMasyarakat');
    Route::POST('/pengaduan-masyarakat/tambah', 'PengaduanMasyarakatController@store')->name('create-PMasyarakat');
    Route::POST('/pengaduan-masyarakat/cari', 'PengaduanMasyarakatController@search')->name('search-PMasyarakat');

    // TIK
    Route::get('/tik', 'TIKController@index')->name('tik');
    Route::POST('/tik/tambah', 'TIKController@store')->name('create-tik');
    Route::POST('/tik/cari', 'TIKController@search')->name('search-tik');
});


Auth::routes();
